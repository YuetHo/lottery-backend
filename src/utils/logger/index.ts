import winston, { format } from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: './logs/all.log' }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    level: 'silly',
    format: format.combine(
      format.errors({ stack: true }),
      format.colorize(),
      format.prettyPrint(),
      format.splat(),
      format.simple(),
      // prettyJson,
    ),
    handleExceptions: true,
  }));
} else {
  logger.add(new winston.transports.Console({
    level: 'info',
    format: format.combine(
      format.errors({ stack: true }),
      format.colorize(),
      format.prettyPrint(),
      format.splat(),
      format.simple(),
      // prettyJson,
    ),
    handleExceptions: true,
  }));
}

export default logger;
