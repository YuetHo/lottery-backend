/* eslint-disable no-unused-vars */
import { Models } from '@database/interface';
import { UserInstance } from '@database/models/User';
import { SessionInstance } from '@database/models/Session';

export interface PermissionGroup {
  [key: string]: 'read' | 'write';
}

export interface Context {
  languageId: string;
  translate: (key: string, option?: any) => string;
  models: Models;
  currentSession?: SessionInstance;
  currentUser?: UserInstance;
}
