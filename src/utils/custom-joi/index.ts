import Joi from 'joi';
import { parseNumber } from 'libphonenumber-js';

const cJoi = Joi.extend((joi: any) => ({
  type: 'phone',
  base: joi.string(),
  // eslint-disable-next-line consistent-return
  validate(value, helpers) {
    const parse: any = parseNumber(value);
    if (!parse.country || !parse.phone) {
      return { value, errors: helpers.error('other.phone') };
    }
  },
})).extend((joi: any) => ({
  type: 'timeRange',
  base: joi.object().keys({
    from: joi.date().max(joi.ref('to')).required(),
    to: joi.date().required(),
  }),
})).extend((joi: any) => ({
  type: 'temporaryFile',
  base: joi.string().regex(/^(\/upload\/temporary\/).*(\.)(.{1,})$/),
}));

export default cJoi;
