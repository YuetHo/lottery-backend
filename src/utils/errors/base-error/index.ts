class BaseError extends Error {
  code: string;

  status: number;

  extras: any;

  message: string;

  constructor(code: string, status: number, extras?: any, message?: string) {
    super(code || 'BaseError');
    this.code = code || 'BaseError';
    this.status = status || 500;
    this.extras = extras || {};
    this.message = message;
  }
}

export default BaseError;
