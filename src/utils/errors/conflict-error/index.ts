import BaseError from '@utils/errors/base-error';

class ConflictError extends BaseError {
  constructor(extras?: any) {
    super('ConflictError', 409, extras);
    Error.captureStackTrace(this, ConflictError);
  }
}

export default ConflictError;
