import BaseError from '@utils/errors/base-error';

class InternalServerError extends BaseError {
  constructor(extras?: any) {
    super('InternalServerError', 500, extras);
    Error.captureStackTrace(this, InternalServerError);
  }
}

export default InternalServerError;
