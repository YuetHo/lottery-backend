import BaseError from '@utils/errors/base-error';

class RequestLimitError extends BaseError {
  constructor(extras?: any) {
    super('RequestLimitError', 429, extras);
    this.code = 'RequestLimitError';
    Error.captureStackTrace(this, RequestLimitError);
  }
}

export default RequestLimitError;
