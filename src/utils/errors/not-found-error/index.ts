import BaseError from '@utils/errors/base-error';

class NotFoundError extends BaseError {
  constructor(extras?: { type: string, resource: string }) {
    super('NotFoundError', 404, extras);
    Error.captureStackTrace(this, NotFoundError);
  }
}

export default NotFoundError;
