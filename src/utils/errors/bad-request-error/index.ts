import BaseError from '@utils/errors/base-error';

class BadRequestError extends BaseError {
  constructor(extras?: any) {
    super('BadRequestError', 400, extras);
    Error.captureStackTrace(this, BadRequestError);
  }
}

export default BadRequestError;
