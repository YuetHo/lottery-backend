class Timer {
  event: string;

  interval: number;

  count: number;

  timerId: ReturnType<typeof setTimeout>;

  constructor(event?: string, interval?: number) {
    this.event = event || 'Undefined event';
    this.interval = interval || 1;
    this.count = 0;
  }

  start() {
    this.timerId = setInterval(() => {
      this.count += 1;
    }, this.interval);
  }

  stop() {
    if (this.timerId) {
      clearInterval(this.timerId);
      console.log(`${this.event} Time used: ${this.count * this.interval} (ms)`);
    }
  }
}

export default Timer;
