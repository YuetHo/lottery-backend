import Crypto from 'crypto';
import Bcrypt from 'bcrypt';
import util from 'util';

const bcryptHashAsync = util.promisify(Bcrypt.hash);
const bcryptCompareAsync = util.promisify(Bcrypt.compare);

const BCRYPT_SALT_DEFAULT_ROUND = 11;

const sha256 = (str: string) => Crypto.createHash('sha256').update(str).digest('hex');
const bcrypt = async (str: string, cost: number = BCRYPT_SALT_DEFAULT_ROUND) => bcryptHashAsync(str, cost);
const bcryptCompare = async (str: string, cryptedStr: string) => bcryptCompareAsync(str, cryptedStr);

export {
  sha256,
  bcrypt,
  bcryptCompare,
};
