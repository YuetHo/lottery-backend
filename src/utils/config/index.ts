import dotenv from 'dotenv';
import path from 'path';

const envDefaultPath = path.resolve(__dirname, '../../../.env.default');
const envPath = path.resolve(__dirname, '../../../.env');

dotenv.config({ path: envPath });
dotenv.config({ path: envDefaultPath });
