interface TrueObj {
  [key: string]: string;
}

export const convertObjToTrueObj = (parentKey : string = '', obj: any) => {
  let trueObj: any = {};
  for (const key of Object.keys(obj)) {
    const trueKey = parentKey ? `${parentKey}.${key}` : key;
    if (!Array.isArray(obj[key]) && typeof obj[key] === 'object' && obj[key]) {
      const keyObj = convertObjToTrueObj(trueKey, obj[key]);
      trueObj = { ...trueObj, ...keyObj };
    } else {
      trueObj[trueKey] = obj[key];
    }
  }
  return trueObj;
};

export const convertTrueObjToObj = (trueObj: TrueObj) => {
  const obj = {};
  for (const trueKey of Object.keys(trueObj)) {
    const keys = trueKey.split('.');
    let cObj: any = obj;
    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      if (i === keys.length - 1) {
        cObj[key] = trueObj[trueKey];
      } else {
        cObj[key] = cObj[key] || {};
        cObj = cObj[key];
      }
    }
  }
  return obj;
};

export default { convertObjToTrueObj, convertTrueObjToObj };
