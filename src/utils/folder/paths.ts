import path from 'path';

const rootPath = path.resolve(__dirname, '../../..');

const folderPaths = {
  temporaryFile: 'upload/temporary/common',
  temporaryImage: 'upload/temporary/image',
  temporaryVideo: 'upload/temporary/video',
  htmlImage: 'upload/html-image',
  userIcon: 'upload/file/user/icon',
  messageImage: 'upload/file/message/image',
};

export { rootPath, folderPaths };
