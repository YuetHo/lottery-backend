import { Sequelize, Op, FindOptions, IncludeOptions, WhereOptions, Utils } from 'sequelize';
import pluralize from 'pluralize';
import { HttpContext } from '@http/interface';
import { Option, Join, Condition, TranslationSetting } from '@actions/list';
import { Models } from '@database/interface';
import translationAction from '@actions/translation';

const getKeyConditions = (queryTargets: { [key: string]: any }[]): {
  [key: string]: {
    [Op.in]: string[] | number[],
  },
} => {
  const selectedKeys: { [key: string]: any[] } = {};
  for (const record of queryTargets) {
    for (const key of Object.keys(record)) {
      const value = record[key];
      if (!selectedKeys[key]) {
        selectedKeys[key] = [];
      }
      selectedKeys[key].push(value);
    }
  }
  const keyConditions: { [key: string]: any } = {};
  for (const key of Object.keys(selectedKeys)) {
    keyConditions[key] = {
      [Op.in]: selectedKeys[key],
    };
  }
  return keyConditions;
};

const getConditionQuery = async (ctx: HttpContext, model: string, condition: Condition): Promise<string> => {
  const { models } = ctx;
  const selectedModel = models[model];
  let { tableName } = selectedModel;
  const { query, getQueryValues, tableNameFormat } = condition;
  if (tableNameFormat === 'singular') {
    tableName = Utils.singularize(tableName);
  } else {
    tableName = Utils.pluralize(tableName);
  }
  const modelRegExp = new RegExp(model, 'g');
  let conditionQuery = query.replace(modelRegExp, tableName);
  if (getQueryValues) {
    const valueObject = await getQueryValues(ctx);
    for (const key of Object.keys(valueObject)) {
      const keyRegExp = new RegExp(`:${key}`, 'g');
      let keyValue = valueObject[key];
      if (typeof keyValue === 'string') {
        keyValue = `'${keyValue}'`;
      }
      conditionQuery = conditionQuery.replace(keyRegExp, keyValue);
    }
  }
  return conditionQuery;
};

const getSequelizeTranslation = (ctx: HttpContext, model: string, translationSetting: TranslationSetting, showDeleted: boolean): IncludeOptions => {
  const { models, languageId } = ctx;
  const { Translation } = models;
  const { required, sourceKey, presetLanguage } = translationSetting;
  const filter: WhereOptions = { model };
  if (presetLanguage) {
    filter.languageId = languageId;
  }
  const translationJoin: IncludeOptions = {
    model: Translation,
    association: models[model].hasMany(Translation,
      {
        foreignKey: 'referenceId', sourceKey: sourceKey || 'id',
      }),
    where: filter,
    required: !!required,
    paranoid: !showDeleted,
  };
  return translationJoin;
};

const getSequelizeJoins = async (ctx: HttpContext, joins: Join[], showDeleted?: boolean): Promise<IncludeOptions[]> => {
  const { models } = ctx;
  const sequelizeJoins = [];
  if (joins) {
    for (const join of joins) {
      const { model, condition, scope, association, required, withTranslation, as, attributes, showDeletedEnable = false } = join;
      const selectedModel = (scope && models[model].scope(scope)) || models[model];
      const joinOption: IncludeOptions = {
        model: selectedModel,
        paranoid: !(showDeletedEnable && showDeleted),
      };
      if (condition) {
        const conditionQuery = await getConditionQuery(ctx, model, condition);
        joinOption.where = {
          [Op.and]: [
            Sequelize.literal(conditionQuery),
          ],
        };
      }
      if (attributes) {
        joinOption.attributes = attributes;
      }
      if (as) {
        joinOption.as = as;
      }
      joinOption.required = !!required;
      if (join.joins) {
        const subJoins = await getSequelizeJoins(ctx, join.joins, showDeleted);
        joinOption.include = subJoins;
      }
      if (withTranslation) {
        const translationJoin = getSequelizeTranslation(ctx, model, withTranslation, showDeletedEnable);
        if (Array.isArray(joinOption.include)) {
          joinOption.include.push(translationJoin);
        } else {
          joinOption.include = [translationJoin];
        }
      }
      if (association) {
        joinOption.association = association(ctx);
      }
      sequelizeJoins.push(joinOption);
    }
  }
  return sequelizeJoins;
};

const getPrimaryKeys = async (models: Models, model: string) => {
  const primaryKeys = await models[model].describe().then((schema: any) => (
    Object.keys(schema).filter((field) => (schema[field].primaryKey))
  ));
  return primaryKeys;
};

const resort = (targets: { [key: string]: any }[], unsortedResults: any[], primaryKeys: string[]) => {
  const getKey = (result: { [key: string]: any }) => {
    const keys = [];
    for (const primaryKey of primaryKeys) {
      keys.push(result[primaryKey]);
    }
    return keys.join('-');
  };

  const obj: { [key: string]: any } = {};
  for (const unsortedResult of unsortedResults) {
    obj[getKey(unsortedResult)] = unsortedResult.toJSON();
  }
  return targets.map((target) => obj[getKey(target)]);
};

const translationFormatter = (ctx: HttpContext, records: any, withTranslation?: TranslationSetting, joins?: Join[]): any[] => {
  const { languageId, models } = ctx;
  let formattedRecords = records;
  if (withTranslation) {
    const { presetLanguage } = withTranslation;
    if (Array.isArray(records)) {
      formattedRecords = [];
      for (const record of records) {
        const formattedRecord = translationAction.translationHandler(record, (presetLanguage && languageId) || undefined);
        formattedRecords.push(formattedRecord);
      }
    } else {
      formattedRecords = translationAction.translationHandler(records, (presetLanguage && languageId) || undefined);
    }
  }
  if (joins) {
    for (const join of joins) {
      const { model } = join;
      const modelName = models[model].name;
      const singleKey = pluralize.singular(modelName);
      const multiKey = pluralize.plural(modelName);
      if (Array.isArray(formattedRecords)) {
        formattedRecords = formattedRecords.map((record) => {
          const formattedRecord = { ...record };
          if (record[multiKey]) {
            formattedRecord[multiKey] = translationFormatter(ctx, record[multiKey], join.withTranslation, join.joins);
          } else if (record[singleKey]) {
            formattedRecord[singleKey] = translationFormatter(ctx, record[singleKey], join.withTranslation, join.joins);
          }
          return formattedRecord;
        });
      } else if (records[multiKey]) {
        formattedRecords[multiKey] = translationFormatter(ctx, formattedRecords[multiKey], join.withTranslation, join.joins);
      } else if (formattedRecords[singleKey]) {
        formattedRecords[singleKey] = translationFormatter(ctx, formattedRecords[singleKey], join.withTranslation, join.joins);
      }
    }
    return formattedRecords;
  }
  return formattedRecords;
};

export default async (ctx: HttpContext, option: Option, query: any, queryTargets: any[]) => {
  const { models } = ctx;
  const { main, transaction } = option;
  const { showDeleted } = query;
  const { model, joins, attributes, withTranslation, scope, showDeletedEnable = false } = main;
  const primaryKeys = await getPrimaryKeys(models, model);
  // Define the sequelize format
  const keyConditions = getKeyConditions(queryTargets);
  const sequelizeOption: FindOptions = { where: keyConditions, include: [], paranoid: !(showDeletedEnable && showDeleted) };
  if (transaction) {
    sequelizeOption.transaction = transaction;
  }
  if (attributes) {
    sequelizeOption.attributes = attributes;
  }
  if (withTranslation) {
    const translationJoin = getSequelizeTranslation(ctx, model, withTranslation, showDeletedEnable && showDeleted);
    if (Array.isArray(sequelizeOption.include)) {
      sequelizeOption.include.push(translationJoin);
    } else {
      sequelizeOption.include = [translationJoin];
    }
  }
  if (joins) {
    const sequelizeJoins = await getSequelizeJoins(ctx, joins, showDeleted);
    if (Array.isArray(sequelizeOption.include)) {
      sequelizeOption.include.push(...sequelizeJoins);
    } else {
      sequelizeOption.include = sequelizeJoins;
    }
  }

  let mainModel = models[model];
  if (scope) {
    mainModel = mainModel.scope(scope);
  }
  let records = await mainModel.findAll(sequelizeOption);
  records = resort(queryTargets, records, primaryKeys);
  records = translationFormatter(ctx, records, withTranslation, joins);
  return records;
};
