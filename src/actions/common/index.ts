import { Transaction, WhereOptions } from 'sequelize/types';
import listAction, { Option } from '@actions/list';
import { NotFoundError } from '@utils/errors';
import { Context } from '@utils/interface';
import { HttpContext } from '@http/interface';

export default {
  getList: (option: Option) => async (ctx: HttpContext) => {
    const result = await listAction.getList(ctx, option);
    ctx.body = result;
  },
  async deleteRecord(ctx: Context, model: string, filter: WhereOptions, scope?: string, transaction?: Transaction) {
    const { models, translate } = ctx;
    let selectedModel = models[model];
    if (scope) {
      selectedModel = selectedModel.scope(scope);
    }
    const record = await selectedModel.findOne({
      where: filter,
    });
    if (!record) {
      throw new NotFoundError({ type: 'resource', resource: translate(selectedModel.name) });
    }
    await record.destroy({ transaction });
  },
};
