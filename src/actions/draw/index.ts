import Draw, { DrawInstance } from '@database/models/Draw';
import sequelize from '@database';
import Ticket from '@database/models/Ticket';
import User from '@database/models/User';
import { Sequelize, Transaction } from 'sequelize';
import Jobs from '@job-queue/job-define';
import initContext from '@utils/context';

const { DEFAULT_LANGUAGE } = process.env;

export default {
  async createDraw(delay: number, transaction?: Transaction): Promise<DrawInstance> {
    const draw = await Draw.create({ status: 'processing' }, { transaction });
    console.log(`Create Draw: ${draw.id}`);
    await Jobs.settlement.add(
      { drawId: draw.id },
      {
        delay,
      },
    );
    return draw;
  },
  settle(drawId: string) {
    return sequelize.transaction(async (transaction: Transaction) => {
      const draw = await Draw.findOne({
        where: { id: drawId, status: 'processing' },
      });
      console.log(`Settle Draw: ${draw.id}`);
      if (draw) {
        const winner = await Ticket.findOne({
          attributes: ['id'],
          where: {
            drawId,
          },
          include: [
            {
              model: User,
              attributes: ['name', 'email', 'phone', 'languageId'],
              required: true,
            },
          ],
          order: [
            Sequelize.fn('RAND'),
          ],
        });
        if (winner) {
          const { id, user } = winner;
          const { name, email, phone, languageId } = user;
          const ctx = await initContext(null, { languageId });
          const { translate } = ctx;
          draw.winnerTicket = id;
          await Jobs.send_email.add({
            receivers: [email],
            subject: translate('winning_email_message_subject'),
            content: translate('winning_email_message_content', { name, ticketId: id, drawId }),
            languageId: languageId || DEFAULT_LANGUAGE,
          });
          await Jobs.send_sms.add({
            phone,
            message: translate('winning_sms_message_content', { name, ticketId: id, drawId }),
          });
        }
        draw.status = 'completed';
        await draw.save({ transaction });
      }
      return draw;
    });
  },
};
