import translationAction from '@actions/translation';
import { QueryInterface } from 'sequelize';

export default {
  async insertRecords(queryInterface: QueryInterface, model: string, modelDefine: string, primaryKey: string, records: any) {
    await queryInterface.bulkInsert(modelDefine, records.map((record: any) => {
      const newRecord = { ...record };
      delete newRecord.translations;
      return newRecord;
    }));
    const translationRecords = [];
    for (const record of records) {
      const { translations } = record;
      if (translations) {
        const recordTranslation = translationAction.convertTranslationRecords(model, record[primaryKey], translations, true);
        translationRecords.push(...recordTranslation);
      }
    }
    if (translationRecords.length) {
      await queryInterface.bulkInsert('translations', translationRecords);
    }
  },

  async removeRecords(queryInterface: QueryInterface, Sequelize: any, model: string, primaryKey: string, records: any) {
    const { Op } = Sequelize;
    const condition: any = {};
    condition[primaryKey] = { [Op.in]: records.map((record: {[key: string]: any}) => record[primaryKey]) };
    await queryInterface.bulkDelete('translations', {
      model,
      referenceId: {
        [Op.in]: records.map((record: {[key: string]: any}) => record[primaryKey]),
      },
    });
  },
};
