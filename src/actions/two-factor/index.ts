import speakeasy from 'speakeasy';
import { RemoteCache } from '@utils/cache';
import { NotFoundError, VerificationCodeIncorrect } from '@utils/errors';
import { Context } from '@utils/interface';

export default {
  async createSecret(ctx: Context, reference: string): Promise<string> {
    const { translate } = ctx;
    const secret = speakeasy.generateSecret({ name: reference });
    await RemoteCache.set('two-fa-secret', reference, secret.base32, { expire: 300 });
    return `${secret.otpauth_url}&issuer=${encodeURIComponent(translate('project_name'))}`;
  },

  async confirmSecret(ctx: Context, reference: string, token: string): Promise<string> {
    const { translate } = ctx;
    const secret = await RemoteCache.get('two-fa-secret', reference);
    if (!secret) {
      throw new NotFoundError({ type: 'two_factor', resource: translate('two_factor') });
    }
    this.verifyToken(secret, token);
    await RemoteCache.del('two-fa-secret', reference);
    return secret;
  },

  verifyToken(secret: string, token: string): boolean {
    const result = speakeasy.totp.verify({ secret, encoding: 'base32', token });
    console.log('Token: ', result, secret, token, typeof token);
    if (!result) {
      throw new VerificationCodeIncorrect();
    }
    return result;
  },
};
