import { bcrypt, bcryptCompare, sha256 } from '@utils/encryptions';

export default {
  async encrypt(text: string): Promise<string> {
    const encryptedText = await bcrypt(sha256(text));
    return encryptedText;
  },

  async verify(text: string, encryptedText: string): Promise<boolean> {
    const result = await bcryptCompare(sha256(text), encryptedText);
    return result;
  },
};
