import { RemoteCache } from '@utils/cache';
import Constant from '@database/models/Constant';

export default {
  async getConstants(): Promise<{
    [key: string]: string
  }> {
    let constants = await RemoteCache.get('constants', 'list');
    if (!constants) {
      constants = await this.updateConstant();
    }
    return constants;
  },
  async updateConstant(): Promise<{
    [key: string]: string
  }> {
    const records = await Constant.findAll();
    const constants: { [key: string]: string } = {};
    for (const record of records) {
      const { key, value } = record;
      constants[key] = value;
    }
    await RemoteCache.set('constants', 'list', constants);
    return constants;
  },
};
