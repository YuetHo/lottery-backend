import fileAction, { ResizeOpt } from '@actions/file';
import initContext from '@utils/context';

class FilePathHandler {
  targetPath: string;

  resize?: ResizeOpt;

  constructor(_targetPath: string, _resize?: ResizeOpt) {
    this.targetPath = _targetPath;
    this.resize = _resize;
  }

  async transfer(filePath: string) {
    const ctx = await initContext();
    const newPath = await fileAction.transferFile(ctx, filePath, this.targetPath, 'move', this.resize);
    return newPath;
  }

  async remove(filePath: string) {
    const ctx = await initContext();
    await fileAction.removeFile(ctx, filePath);
    return filePath;
  }
}

export default FilePathHandler;
