import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database/index';
import User, { UserInstance } from '@database/models/User';
import Draw from '@database/models/Draw';

export interface TicketInstance extends Model {
  id: string;
  drawId: string;
  userId: string;
  createdAt: Date;
  updatedAt: Date;
  user ?: UserInstance;
}

const Ticket = sequelize.define<TicketInstance>('tickets', {
  id: {
    allowNull: false,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
    type: DataTypes.UUID,
  },
  drawId: {
    allowNull: false,
    type: DataTypes.BIGINT,
    references: {
      model: 'draws',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  userId: {
    allowNull: false,
    type: DataTypes.STRING,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Draw.hasMany(Ticket, {
  foreignKey: 'drawId',
  sourceKey: 'id',
});
Ticket.belongsTo(Draw, {
  foreignKey: 'drawId',
  targetKey: 'id',
});
User.hasMany(Ticket, {
  foreignKey: 'userId',
  sourceKey: 'id',
});
Ticket.belongsTo(User, {
  foreignKey: 'userId',
  targetKey: 'id',
});

export default Ticket;
