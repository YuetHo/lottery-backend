import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';

export interface VerificationInstance extends Model {
  type: 'phone' | 'email' | 'twoFA';
  reference: string;
  authCode: string;
  token: string;
  isVerified: boolean;
  userId: string;
  expiresAt: Date;
  createdAt: Date;
  updatedAt: Date;
}

const Verification = sequelize.define<VerificationInstance>('verifications', {
  type: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.ENUM('phone', 'email', 'twoFA'),
  },
  reference: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  authCode: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  token: {
    allowNull: false,
    defaultValue: DataTypes.UUIDV4,
    type: DataTypes.UUID,
  },
  isVerified: {
    allowNull: false,
    defaultValue: false,
    type: DataTypes.BOOLEAN,
  },
  userId: {
    allowNull: true,
    type: DataTypes.STRING,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  expiresAt: {
    allowNull: false,
    type: DataTypes.DATE,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Verification;
