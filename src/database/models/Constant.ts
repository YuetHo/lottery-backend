import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database/index';

export interface ConstantInstance extends Model {
  key: string;
  value: string;
  type: 'string' | 'number' | 'boolean';
  createdAt: Date;
  updatedAt: Date;
}

const Constant = sequelize.define<ConstantInstance>('constants', {
  key: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  value: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  type: {
    allowNull: false,
    defaultValue: 'string',
    type: DataTypes.ENUM('string', 'number', 'boolean'),
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Constant;
