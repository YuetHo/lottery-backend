/* eslint-disable camelcase */
import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import { NotificationContentInstance } from '@database/models/NotificationContent';
import { NotificationTargetInstance } from '@database/models/NotificationTarget';

export interface NotificationInstance extends Model {
  id: number;
  isSystem: boolean;
  pushedAt: Date;
  createdAt: Date;
  updatedAt: Date;
  notification_contents?: NotificationContentInstance[];
  notification_target?: NotificationTargetInstance[];
}

const Notification = sequelize.define<NotificationInstance>('notifications', {
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.BIGINT,
  },
  isSystem: {
    allowNull: false,
    defaultValue: false,
    type: DataTypes.BOOLEAN,
  },
  pushedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Notification;
