import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Language, { LanguageInstance } from '@database/models/Language';
import User, { UserInstance } from '@database/models/User';

export interface FCMInstance extends Model {
  key: string;
  languageId: string;
  userId: string;
  unreadCount: number;
  enabled: boolean;
  createdAt: Date;
  updatedAt: Date;
  language?: LanguageInstance;
  user?: UserInstance;
}

const FCM = sequelize.define<FCMInstance>('fcms', {
  key: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  languageId: {
    allowNull: false,
    type: DataTypes.STRING,
    references: {
      model: 'languages',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  userId: {
    allowNull: true,
    type: DataTypes.STRING,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  unreadCount: {
    allowNull: false,
    defaultValue: 0,
    type: DataTypes.INTEGER,
  },
  enabled: {
    allowNull: false,
    defaultValue: true,
    type: DataTypes.BOOLEAN,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Language.hasMany(FCM, {
  foreignKey: 'languageId',
  sourceKey: 'id',
});
FCM.belongsTo(Language, {
  foreignKey: 'languageId',
  targetKey: 'id',
});
User.hasMany(FCM, {
  foreignKey: 'userId',
  sourceKey: 'id',
});
FCM.belongsTo(User, {
  foreignKey: 'userId',
  targetKey: 'id',
});

export default FCM;
