import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database/index';

export interface DrawInstance extends Model {
  id: string;
  status: 'pending' | 'processing' | 'completed'
  winnerTicket: string;
  createdAt: Date;
  updatedAt: Date;
}

const Draw = sequelize.define<DrawInstance>('draws', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.BIGINT,
  },
  status: {
    allowNull: false,
    type: DataTypes.ENUM('pending', 'processing', 'completed'),
  },
  winnerTicket: {
    allowNull: true,
    type: DataTypes.UUID,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Draw;
