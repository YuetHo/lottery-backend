import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Permission, { PermissionInstance } from '@database/models/Permission';
import Session, { SessionInstance } from '@database/models/Session';

export interface SessionPermissionInstance extends Model {
  permissionId: string;
  sessionId: number;
  right: 'read' | 'write' | 'banned';
  createdAt: Date;
  updatedAt: Date;
  permission?: PermissionInstance;
  session?: SessionInstance;
}

const SessionPermission = sequelize.define<SessionPermissionInstance>('session_permissions', {
  permissionId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
    references: {
      model: 'permissions',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  sessionId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.BIGINT,
    references: {
      model: 'sessions',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  right: {
    allowNull: false,
    type: DataTypes.ENUM('read', 'write', 'banned'),
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Permission.hasMany(SessionPermission, {
  foreignKey: 'permissionId',
  sourceKey: 'id',
});
SessionPermission.belongsTo(Permission, {
  foreignKey: 'permissionId',
  targetKey: 'id',
});
Session.hasMany(SessionPermission, {
  foreignKey: 'sessionId',
  sourceKey: 'id',
});
SessionPermission.belongsTo(Session, {
  foreignKey: 'sessionId',
  targetKey: 'id',
});

export default SessionPermission;
