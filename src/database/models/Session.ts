/* eslint-disable camelcase */
import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import User, { UserInstance } from '@database/models/User';
import { SessionPermissionInstance } from '@database/models/SessionPermission';

export interface SessionInstance extends Model {
  id: number;
  userId: string;
  accessToken: string;
  refreshToken: string;
  type: 'login' | 'api' | 'oauth';
  ip: string;
  browser: string;
  os: string;
  expiresAt: Date;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  user?: UserInstance;
  session_permissions?: SessionPermissionInstance;
}

const Session = sequelize.define<SessionInstance>('sessions', {
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.BIGINT,
  },
  userId: {
    allowNull: false,
    type: DataTypes.STRING,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  accessToken: {
    allowNull: false,
    defaultValue: DataTypes.UUIDV4,
    type: DataTypes.UUID,
  },
  refreshToken: {
    allowNull: false,
    defaultValue: DataTypes.UUIDV4,
    type: DataTypes.UUID,
  },
  type: {
    allowNull: false,
    defaultValue: 'login',
    type: DataTypes.ENUM('login', 'api', 'oauth'),
  },
  ip: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  browser: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  os: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  expiresAt: {
    allowNull: false,
    type: DataTypes.DATE,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  deletedAt: {
    allowNull: true,
    type: DataTypes.DATE,
  },
});

User.hasMany(Session, {
  foreignKey: 'userId',
  sourceKey: 'id',
});
Session.belongsTo(User, {
  foreignKey: 'userId',
  targetKey: 'id',
});

export default Session;
