import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import { FCMInstance } from '@database/models/FCM';
import { TranslationInstance } from '@database/models/Translation';

export interface LanguageInstance extends Model {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  fcms?: FCMInstance[];
  translations?: TranslationInstance;
}

const Language = sequelize.define<LanguageInstance>('languages', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Language;
