import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';

export interface ResourceInstance extends Model {
  id: number;
  path: string;
  method: string;
  type: 'api' | 'file';
  permissions: JSON;
  active: boolean;
  createdAt: Date;
  updatedAt: Date;
}

const Resource = sequelize.define<ResourceInstance>('resources', {
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.BIGINT,
  },
  path: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  method: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  type: {
    allowNull: false,
    type: DataTypes.ENUM('api', 'file'),
  },
  permissions: {
    allowNull: true,
    type: DataTypes.JSON,
  },
  active: {
    allowNull: false,
    defaultValue: true,
    type: DataTypes.BOOLEAN,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
}, {
  indexes: [
    {
      unique: true,
      fields: ['path', 'method'],
    },
  ],
});

export default Resource;
