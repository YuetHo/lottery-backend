import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Permission, { PermissionInstance } from '@database/models/Permission';
import Role, { RoleInstance } from '@database/models/Role';

export interface RolePermissionInstance extends Model {
  permissionId: string;
  roleId: number;
  right: 'read' | 'write';
  createdAt: Date;
  updatedAt: Date;
  role?: RoleInstance;
  permission?: PermissionInstance;
}

const RolePermission = sequelize.define<RolePermissionInstance>('role_permissions', {
  permissionId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
    references: {
      model: 'permissions',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  roleId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.BIGINT,
    references: {
      model: 'roles',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  right: {
    allowNull: false,
    type: DataTypes.ENUM('read', 'write'),
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

RolePermission.belongsTo(Permission, {
  foreignKey: 'permissionId',
  targetKey: 'id',
});
Permission.hasMany(RolePermission, {
  foreignKey: 'permissionId',
  sourceKey: 'id',
});
RolePermission.belongsTo(Role, {
  foreignKey: 'roleId',
  targetKey: 'id',
});
Role.hasMany(RolePermission, {
  foreignKey: 'roleId',
  sourceKey: 'id',
});

export default RolePermission;
