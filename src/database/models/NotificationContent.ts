import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Notification, { NotificationInstance } from '@database/models/Notification';
import Message, { MessageInstance } from '@database/models/Message';

export interface NotificationContentInstance extends Model {
  notificationId: number;
  type: 'sms' | 'email' | 'inbox' | 'push';
  messageId: string;
  messageParams: JSON;
  createdAt: Date;
  updatedAt: Date;
  notification?: NotificationInstance;
  message?: MessageInstance;
}

const NotificationContent = sequelize.define<NotificationContentInstance>('notification_contents', {
  notificationId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.BIGINT,
    references: {
      model: 'notifications',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  type: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.ENUM('sms', 'email', 'inbox', 'push'),
  },
  messageId: {
    allowNull: false,
    type: DataTypes.STRING,
    references: {
      model: 'messages',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  messageParams: {
    allowNull: true,
    type: DataTypes.JSON,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Notification.hasMany(NotificationContent, {
  foreignKey: 'notificationId',
  sourceKey: 'id',
});
NotificationContent.belongsTo(Notification, {
  foreignKey: 'notificationId',
  targetKey: 'id',
});
Message.hasMany(NotificationContent, {
  foreignKey: 'messageId',
  sourceKey: 'id',
});
NotificationContent.belongsTo(Message, {
  foreignKey: 'messageId',
  targetKey: 'id',
});

export default NotificationContent;
