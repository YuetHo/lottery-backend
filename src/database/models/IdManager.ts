import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';

export interface IdManagerInstance extends Model {
  idHead: string;
  idBody: number;
  createdAt: Date;
  UpdatedAt: Date;
  getNewId: Function;
}

const IdManager = sequelize.define<IdManagerInstance>('id_managers', {
  idHead: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  idBody: {
    allowNull: false,
    defaultValue: 0,
    type: DataTypes.INTEGER.UNSIGNED,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default IdManager;
