/* eslint-disable camelcase */
import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import { SessionPermissionInstance } from '@database/models/SessionPermission';
import { RolePermissionInstance } from '@database/models/RolePermission';
import { UserPermissionInstance } from '@database/models/UserPermission';

export interface PermissionInstance extends Model {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  session_permissions?: SessionPermissionInstance;
  role_permissions?: RolePermissionInstance;
  user_permissions?: UserPermissionInstance;
}

const Permission = sequelize.define<PermissionInstance>('permissions', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Permission;
