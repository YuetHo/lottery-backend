import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';

export interface QueryInstance extends Model {
  id: number;
  type: 'userId';
  content: string;
  variable: {
    [key: string]: 'string' | 'number' | 'boolean';
  };
  createdAt: Date;
  updatedAt: Date;
}

const Query = sequelize.define<QueryInstance>('queries', {
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.BIGINT,
  },
  type: {
    allowNull: false,
    type: DataTypes.ENUM('userId'),
  },
  content: {
    allowNull: false,
    type: DataTypes.TEXT,
  },
  variable: {
    allowNull: true,
    type: DataTypes.JSON,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Query;
