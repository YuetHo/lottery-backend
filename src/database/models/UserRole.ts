import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Role, { RoleInstance } from '@database/models/Role';
import User, { UserInstance } from '@database/models/User';

export interface UserRoleInstance extends Model {
  userId: string;
  roleId: number;
  createdAt: Date;
  updatedAt: Date;
  role?: RoleInstance;
  user?: UserInstance;
}

const UserRole = sequelize.define<UserRoleInstance>('user_roles', {
  userId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  roleId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.BIGINT,
    references: {
      model: 'roles',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Role.hasMany(UserRole, {
  foreignKey: 'roleId',
  sourceKey: 'id',
});
UserRole.belongsTo(Role, {
  foreignKey: 'roleId',
  targetKey: 'id',
});
User.hasMany(UserRole, {
  foreignKey: 'userId',
  sourceKey: 'id',
});
UserRole.belongsTo(User, {
  foreignKey: 'userId',
  targetKey: 'id',
});

export default UserRole;
