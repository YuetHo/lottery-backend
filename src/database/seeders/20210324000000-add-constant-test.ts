import 'module-alias/register';
import seederAction from '@actions/seeder';
import { QueryInterface } from 'sequelize';

const model = 'Constant';
const modelDefine = 'constants';
const primaryKey = 'key';
const records = [
  {
    key: 'test_1',
    value: '5',
    type: 'number',
    translations: {
      'en-US': {
        title: 'Test 1',
      },
      'zh-HK': {
        title: 'Test 1 HK',
      },
    },
  },
  {
    key: 'test_2',
    value: 'true',
    type: 'boolean',
    translations: {
      'en-US': {
        title: 'Test 2',
      },
      'zh-HK': {
        title: 'Test 2 HK',
      },
    },
  },
  {
    key: 'test_3',
    value: 'abc',
    type: 'string',
    translations: {
      'en-US': {
        title: 'Test 3',
      },
      'zh-HK': {
        title: 'Test 3 HK',
      },
    },
  },
];

export const up = async (queryInterface: QueryInterface) => seederAction.insertRecords(queryInterface, model, modelDefine, primaryKey, records);
export const down = async (queryInterface: QueryInterface, Sequelize: any) => seederAction.removeRecords(queryInterface, Sequelize, model, primaryKey, records);
