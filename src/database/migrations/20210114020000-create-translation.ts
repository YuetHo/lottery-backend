import { DataTypes, QueryInterface, Sequelize } from 'sequelize';

export const up = async (queryInterface: QueryInterface) => {
  await queryInterface.createTable('translations', {
    model: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING(126),
    },
    referenceId: {
      allowNull: true,
      primaryKey: true,
      defaultValue: 'null',
      type: DataTypes.STRING(255),
    },
    languageId: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING(126),
      references: {
        model: 'languages',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    field: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING(126),
    },
    content: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    textContent: {
      allowNull: true,
      type: DataTypes.TEXT,
    },
    variable: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    createdAt: {
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      type: DataTypes.DATE,
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE,
    },
  });
  await queryInterface.addIndex('translations', ['model', 'referenceId', 'field']);
};
export const down = async (queryInterface: QueryInterface) => {
  await queryInterface.dropTable('translations');
};
