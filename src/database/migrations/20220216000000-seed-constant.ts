import 'module-alias/register';
import seederAction from '@actions/seeder';
import { QueryInterface } from 'sequelize';

const model = 'Constant';
const modelDefine = 'constants';
const primaryKey = 'key';
const records = [
  {
    key: 'draw_period',
    value: '300',
    type: 'number',
    translations: {
      'en-US': {
        title: 'Draw Period',
      },
      'zh-HK': {
        title: '期間時長',
      },
      'zh-CN': {
        title: '期间时长',
      },
    },
  },
  // {
  //   key: 'commission',
  //   value: '20',
  //   type: 'number',
  //   translations: {
  //     'en-US': {
  //       title: 'Commission(%)',
  //     },
  //     'zh-HK': {
  //       title: '提成(%)',
  //     },
  //   },
  // },
];

export const up = async (queryInterface: QueryInterface) => seederAction.insertRecords(queryInterface, model, modelDefine, primaryKey, records);
export const down = async (queryInterface: QueryInterface, Sequelize: any) => seederAction.removeRecords(queryInterface, Sequelize, model, primaryKey, records);
