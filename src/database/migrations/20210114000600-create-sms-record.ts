import { DataTypes, QueryInterface } from 'sequelize';

export const up = async (queryInterface: QueryInterface) => {
  await queryInterface.createTable('sms_counts', {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    provider: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    count: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    date: {
      allowNull: false,
      type: DataTypes.DATEONLY,
    },
  });
  await queryInterface.addIndex('sms_counts', ['provider', 'date'], { unique: true });
};
export const down = async (queryInterface: QueryInterface) => {
  await queryInterface.dropTable('sms_counts');
};
