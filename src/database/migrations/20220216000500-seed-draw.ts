import 'module-alias/register';
import drawAction from '@actions/draw';
import constantAction from '@actions/constant';

export const up = async () => {
  const constant = await constantAction.getConstants();
  await drawAction.createDraw(Number(constant.draw_period) * 1000);
};
export const down = async () => {};
