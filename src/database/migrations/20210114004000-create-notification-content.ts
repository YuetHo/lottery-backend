import { Sequelize, DataTypes, QueryInterface } from 'sequelize';

export const up = async (queryInterface: QueryInterface) => {
  await queryInterface.createTable('notification_contents', {
    notificationId: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.BIGINT,
      references: {
        model: 'notifications',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    type: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.ENUM('sms', 'email', 'inbox', 'push'),
    },
    messageId: {
      allowNull: false,
      type: DataTypes.STRING,
      references: {
        model: 'messages',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    messageParams: {
      allowNull: true,
      type: DataTypes.JSON,
    },
    createdAt: {
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      type: DataTypes.DATE,
    },
  });
};
export const down = async (queryInterface: QueryInterface) => {
  await queryInterface.dropTable('notification_contents');
};
