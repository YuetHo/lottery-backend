import 'module-alias/register';
import seederAction from '@actions/seeder';
import { QueryInterface } from 'sequelize';

const model = 'Role';
const modelDefine = 'roles';
const primaryKey = 'id';
const records = [
  {
    id: 1,
    translations: {
      'en-US': {
        title: 'Contestant',
      },
      'zh-HK': {
        title: '參加者',
      },
      'zh-CN': {
        title: '参加者',
      },
    },
  },
];

export const up = async (queryInterface: QueryInterface) => seederAction.insertRecords(queryInterface, model, modelDefine, primaryKey, records);
export const down = async (queryInterface: QueryInterface, Sequelize: any) => seederAction.removeRecords(queryInterface, Sequelize, model, primaryKey, records);
