import { HttpContext } from '@http/interface';
import Draw from '@database/models/Draw';
import Ticket from '@database/models/Ticket';
import { NotFoundError, TicketAlreadyBought } from '@utils/errors';
import sequelize from '@database';
import { Transaction } from 'sequelize/types';

export default {
  buyTicket: async (ctx: HttpContext) => {
    const { translate, currentUser } = ctx;
    const currentDraw = await Draw.findOne({
      where: { status: 'processing' },
      order: [['createdAt', 'DESC']],
    });
    if (!currentDraw) {
      throw new NotFoundError({ type: 'draw', resource: translate('draw') });
    }
    return sequelize.transaction(async (transaction: Transaction) => {
      const existTicket = await Ticket.findOne({
        where: {
          drawId: currentDraw.id,
          userId: currentUser.id,
        },
      });
      if (existTicket) {
        throw new TicketAlreadyBought();
      }
      const ticket = await Ticket.create({
        userId: currentUser.id,
        drawId: currentDraw.id,
      }, { transaction });
      ctx.body = {
        id: ticket.id,
      };
    });
  },
};
