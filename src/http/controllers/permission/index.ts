import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import sequelize from '@database';
import commonAction from '@actions/common';
import permissionAction from '@actions/permission';
import translationAction from '@actions/translation';
import UserPermission from '@database/models/UserPermission';
import RolePermission from '@database/models/RolePermission';
import Permission from '@database/models/Permission';
import { Transaction } from 'sequelize/types';
import { RemoteCache } from '@utils/cache';
import cJoi from '@utils/custom-joi';

export default {
  getPermissions: commonAction.getList({
    main: {
      model: 'Permission',
      withTranslation: {},
    },
    withCount: false,
    sorts: {
      title: 'PermissionTranslation.title',
    },
  }),

  getPermissionList: commonAction.getList({
    main: {
      model: 'Permission',
      withTranslation: {},
    },
    sorts: {
      id: 'Permission.id',
      title: 'PermissionTranslation.title',
      createdAt: 'Permission.createdAt',
      updatedAt: 'Permission.updatedAt',
    },
    search: ['Permission.id', 'PermissionTranslation.title'],
    filter: {
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'Permission.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Permission.updatedAt',
      },
    },
  }),

  getUserPermissions: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { userId } = params;
    let userPermissions: any = await permissionAction.getUserPermissions(ctx, userId);
    if (userPermissions[userId]) {
      userPermissions = permissionAction.arrayToObject([userPermissions[userId]]);
    }
    ctx.body = userPermissions || {};
  },

  assignUserPermissions: compose([
    apiValidator.body({
      permissions: Joi.array().items(Joi.object().keys({
        permissionId: Joi.string().required(),
        right: Joi.string().valid('read', 'write', 'banned').required(),
      })).required(),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData } = ctx;
      const { userId } = params;
      const { permissions } = validatedData?.body;
      return sequelize.transaction(async (transaction: Transaction) => {
        await UserPermission.destroy({
          where: {
            userId,
          },
        });
        for (let i = 0; i < permissions.length; i += 1) {
          permissions[i].userId = userId;
        }
        await UserPermission.bulkCreate(permissions, { transaction });
        await RemoteCache.del('user-permissions', userId);
        ctx.body = {};
      });
    },
  ]),

  getRolePermissions: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { roleId } = params;
    let rolePermissions: any = await permissionAction.getRolePermissions(ctx, [roleId]);
    if (rolePermissions[roleId]) {
      rolePermissions = permissionAction.arrayToObject(rolePermissions[roleId]);
    }
    ctx.body = rolePermissions || {};
  },

  assignRolePermissions: compose([
    apiValidator.body({
      permissions: Joi.array().items(Joi.object().keys({
        permissionId: Joi.string().required(),
        right: Joi.string().required(),
      })).required(),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData } = ctx;
      const { roleId } = params;
      const { permissions } = validatedData?.body;
      return sequelize.transaction(async (transaction: Transaction) => {
        await RolePermission.destroy({
          where: {
            roleId,
          },
        });
        for (let i = 0; i < permissions.length; i += 1) {
          permissions[i].roleId = roleId;
        }
        await RolePermission.bulkCreate(permissions, { transaction });
        await RemoteCache.del('role-permissions', roleId);
        ctx.body = {};
      });
    },
  ]),

  createPermission: compose([
    apiValidator.body({
      id: Joi.string().required(),
      translations: Joi.object().pattern(/[-\w]/, {
        title: Joi.string().required(),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const body = validatedData?.body;
      const translations = { ...body.translations };
      delete body.translations;
      return sequelize.transaction(async (transaction: Transaction) => {
        const permission = await Permission.create(body, { transaction });
        await translationAction.upsertTranslation(ctx, {
          model: 'Permission',
          referenceId: permission.id,
        }, translations, transaction);
        ctx.body = permission;
      });
    },
  ]),

  updatePermission: compose([
    apiValidator.body({
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData } = ctx;
      const { permissionId } = params;
      const body = validatedData?.body;
      const { translations } = body;
      return sequelize.transaction(async (transaction: Transaction) => {
        await translationAction.upsertTranslation(ctx, {
          model: 'Permission',
          referenceId: permissionId,
        }, translations, transaction);
        ctx.body = {};
      });
    },
  ]),

  deletePermission: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { permissionId } = params;
    return sequelize.transaction(async (transaction) => {
      await commonAction.deleteRecord(ctx, 'Permission', {
        id: permissionId,
      }, undefined, transaction);
      await translationAction.deleteTranslation({
        model: 'Permission',
        referenceId: permissionId,
      }, transaction);
      ctx.body = {};
    });
  },
};
