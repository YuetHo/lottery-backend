import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import sequelize from '@database';
import translationAction from '@actions/translation';
import Translation from '@database/models/Translation';
import { convertTrueObjToObj } from '@utils/object-convertor';

const AllSupportPlatform: string[] = process.env.SUPPORTED_PLATFORM!.split(',');
const supportPlatforms: string[] = process.env.SUPPORTED_PLATFORM!.split(',');

export default {
  getSupportedPlatforms: async (ctx: HttpContext) => {
    ctx.body = process.env.SUPPORTED_PLATFORM!.split(',');
  },

  getTranslationSource: compose([
    apiValidator.query({
      platform: Joi.string().valid(...supportPlatforms).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const query = validatedData!.query!;
      const { platform } = query;
      const defaultSources: { [key: string]: any } = await translationAction.getDefaultSource([platform])!;
      const dbSources = await translationAction.getDBSource([platform]);
      const translationSource = convertTrueObjToObj({ ...defaultSources[platform], ...dbSources[platform] });
      ctx.body = translationSource;
    },
  ]),

  updateDefaultTranslationSource: compose([
    apiValidator.body({
      platform: Joi.string().valid(...supportPlatforms).required(),
      source: Joi.object().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { platform, source } = validatedData?.body;
      translationAction.updateDefaultTranslationSource(platform, source);
      ctx.body = {};
    },
  ]),

  getTranslationsList: compose([
    apiValidator.query({
      page: Joi.number().min(1).required(),
      sort: Joi.array().items(Joi.object({
        orderBy: Joi.string().valid('platform', 'field').required(),
        direction: Joi.string().valid('ASC', 'DESC').required(),
      })).required(),
      limit: Joi.number().min(1).required(),
      languageId: Joi.string(),
      search: Joi.string(),
      filter: Joi.object().keys({
        platform: Joi.string().valid(...AllSupportPlatform),
        field: Joi.string(),
      }),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const query = validatedData?.query;
      const { page, sort, limit, search, filter } = query;
      const { orderBy, direction } = sort[0];
      const listRecords = await translationAction.getList(ctx, page, orderBy, direction, limit, search, filter);
      ctx.body = listRecords;
    },
  ]),

  updateTranslation: compose([
    apiValidator.params({
      platform: Joi.string().valid(...supportPlatforms).required(),
      field: Joi.string(),
    }),
    apiValidator.body({
      translations: Joi.array().items(Joi.object().keys({
        languageId: Joi.string().required(),
        content: Joi.string().required(),
      })),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, params } = ctx;
      const { translations } = validatedData?.body;
      const { platform, field } = params;
      await sequelize.transaction(async (transaction) => {
        const newRecords = [];
        const dbRecords = await Translation.findAll({ where: { model: platform, field }, transaction });
        for (const translation of translations) {
          const { languageId, content } = translation;
          let created = false;
          for (const dbRecord of dbRecords) {
            if (dbRecord.languageId === languageId) {
              created = true;
              dbRecord.content = content;
              await dbRecord.save({ transaction });
            }
          }
          if (!created) {
            newRecords.push({
              model: platform,
              field,
              referenceId: 'null',
              languageId,
              content,
            });
          }
        }
        await Translation.bulkCreate(newRecords, { transaction });
      });
      await translationAction.getDBSource([platform], false);
      ctx.body = {};
    },
  ]),
};
