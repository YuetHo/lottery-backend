import compose from 'koa-compose';
import Joi from 'joi';
import apiValidator from '@http/middleware/api-validator';
import cJoi from '@utils/custom-joi';
import { HttpContext } from '@http/interface';
import constantAction from '@actions/constant';
import Constant from '@database/models/Constant';
import sequelize from '@database';
import { Transaction } from 'sequelize/types';
import commonAction from '@actions/common';
import translationAction from '@actions/translation';
import { NotFoundError } from '@utils/errors';

export default {
  getConstantList: commonAction.getList({
    main: {
      model: 'Constant',
      withTranslation: { sourceKey: 'key' },
    },
    sorts: {
      key: 'Constant.key',
      title: 'ConstantTranslation.title',
      type: 'Constant.type',
    },
    search: ['Constant.key', 'ConstantTranslation.title'],
    filter: {
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'Constant.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Constant.updatedAt',
      },
    },
  }),

  updateConstant: compose([
    apiValidator.body({
      value: Joi.string(),
      translations: Joi.object().pattern(/[-w]+/, {
        title: Joi.string().required(),
      }),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData, translate } = ctx;
      const { constantKey } = params;
      const body = validatedData?.body;
      const { translations } = body;
      delete body.translations;
      await sequelize.transaction(async (transaction: Transaction) => {
        const constant = await Constant.findOne({
          where: {
            key: constantKey,
          },
        });
        if (!constant) {
          throw new NotFoundError({ type: 'constant', resource: translate('constant') });
        }
        Object.assign(constant, body);
        await constant.save({ transaction });
        if (translations) {
          await translationAction.upsertTranslation(ctx, {
            model: 'Constant',
            referenceId: constantKey,
          }, translations, transaction);
        }
      });
      ctx.body = await constantAction.updateConstant();
    },
  ]),
};
