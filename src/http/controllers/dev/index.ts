import Joi from 'joi';
import { HttpContext } from '@http/interface';
import compose from 'koa-compose';
import apiValidator from '@http/middleware/api-validator';
import User from '@database/models/User';
import drawAction from '@actions/draw';
import Draw from '@database/models/Draw';

export default {
  deleteContestant: compose([
    apiValidator.query({
      email: Joi.string().email().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { query } = validatedData;
      const { email } = query;
      await User.destroy({
        where: {
          email,
        },
        force: true,
      });
      ctx.body = {};
    },
  ]),
  createDraw: compose([
    apiValidator.body({
      delay: Joi.number().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { body } = validatedData;
      const { delay } = body;
      const draw = await drawAction.createDraw(delay);
      ctx.body = { draw };
    },
  ]),
  settleDraw: compose([
    apiValidator.body({
      drawId: Joi.number().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { body } = validatedData;
      const { drawId } = body;
      const draw = await drawAction.settle(drawId);
      ctx.body = { draw };
    },
  ]),
  deleteDraw: compose([
    apiValidator.query({
      drawId: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { query } = validatedData;
      const { drawId } = query;
      await Draw.destroy({
        where: {
          id: drawId,
        },
        force: true,
      });
      ctx.body = {};
    },
  ]),
};
