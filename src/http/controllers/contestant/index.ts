import compose from 'koa-compose';
import Joi from 'joi';
import sequelize from '@database';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import cJoi from '@utils/custom-joi';
import userAction from '@actions/user';
import User from '@database/models/User';
import UserRole from '@database/models/UserRole';
import { Op, Transaction } from 'sequelize';
import { IncorrectPassword, InfoAlreadyUsed, NotFoundError } from '@utils/errors';
import encryptAction from '@actions/encrypt';
import sessionAction from '@actions/session';
import Session from '@database/models/Session';
import { RemoteCache } from '@utils/cache';

export default {
  register: compose([
    apiValidator.body({
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      phone: cJoi.string().required(),
      password: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { translate, validatedData } = ctx;
      const { body } = validatedData;
      const { email, phone } = body;
      return sequelize.transaction(async (transaction: Transaction) => {
        const existUser = await User.findOne({
          where: {
            [Op.or]: {
              email,
              phone,
            },
          },
        });
        if (existUser) {
          if (existUser.phone === phone) {
            throw new InfoAlreadyUsed({ info: translate('phone') });
          } else {
            throw new InfoAlreadyUsed({ info: translate('email') });
          }
        }
        const contestant = await userAction.createUser(body, transaction);
        await UserRole.create({
          userId: contestant.id,
          roleId: 1,
        }, { transaction });
        ctx.body = {};
      });
    },
  ]),

  login: compose([
    apiValidator.body({
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { translate, validatedData, userAgent, request, languageId } = ctx;
      const { body } = validatedData;
      const { ip } = request;
      const { email, password } = body;
      const user = await User.scope('all').findOne({
        where: {
          email,
        },
      });
      if (user) {
        const result = await encryptAction.verify(password, user.password);
        if (!result) {
          throw new IncorrectPassword();
        }
      } else {
        throw new NotFoundError({ type: 'user', resource: translate('user') });
      }
      return sequelize.transaction(async (transaction: Transaction) => {
        if (user.languageId !== languageId) {
          user.languageId = languageId;
          await user.save({ transaction });
        }
        const session = await sessionAction.createSession({
          userId: user.id,
          type: 'login',
          browser: userAgent.browser,
          os: userAgent.os,
          ip: ip.replace('::ffff:', ''),
        }, transaction);
        ctx.body = {
          accessToken: session.accessToken,
          refreshToken: session.refreshToken,
          expiresAt: session.expiresAt,
        };
      });
    },
  ]),

  logout: async (ctx: HttpContext) => {
    const { currentSession } = ctx;
    if (currentSession) {
      const { accessToken } = currentSession;
      sequelize.transaction(async (transaction: Transaction) => {
        await Session.destroy({
          where: {
            accessToken,
          },
          transaction,
        });
        await RemoteCache.del('sessions', accessToken);
      });
    }
    ctx.body = {};
  },
};
