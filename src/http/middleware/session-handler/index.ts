import { Next } from 'koa';
import sessionAction from '@actions/session';
import { LoginRequired } from '@utils/errors';
import { ApiRecord, HttpContext } from '@http/interface';

const sessionHandler = (apiDetail?: ApiRecord) => async (ctx: HttpContext, next: Next) => {
  const { request } = ctx;
  const { header } = request;
  const cookieAuth = ctx.cookies.get('Authorization');
  if (header.authorization) {
    const session = await sessionAction.getSessionByAccessToken(ctx, header.authorization);

    if (session) {
      ctx.currentSession = session;
      ctx.currentUser = session.user;
    }
  }
  if (!ctx.currentSession && cookieAuth) {
    const cookieSession = await sessionAction.getSessionByAccessToken(ctx, cookieAuth);
    if (cookieSession) {
      ctx.cookieSession = cookieSession;
      ctx.cookieUser = cookieSession.user;
    }
  }
  if (apiDetail?.sessionRequire && !ctx.currentSession) {
    throw new LoginRequired();
  }
  await next();
};

export default sessionHandler;
