import { Next } from 'koa';
import { PermissionDenied, PrivateFileProtected, LoginRequired } from '@utils/errors';
import permissionAction from '@actions/permission';
import { ApiRecord, HttpContext } from '@http/interface';
import sequelize from '@database';

const permissionHandler = (apiDetail?: ApiRecord) => async (ctx: HttpContext, next: Next) => {
  const { request, currentSession, currentUser, cookieSession, cookieUser, translate } = ctx;
  const { method, url } = request;
  const path = (url.includes('?') && url.split('?')[0]) || url;
  const isUploadFile = path.includes('/upload/');
  if (isUploadFile) {
    const isPrivateFile = path.includes('/private/');
    if (isPrivateFile) {
      const requestUser = currentUser || cookieUser;
      if (!requestUser) {
        throw new LoginRequired();
      }
      // private user file example
      const pathParameters = path.split('/');
      const userIndex = pathParameters.findIndex((parameter: string) => parameter === 'user');
      let isOwner = false;
      if (pathParameters.length > userIndex) {
        const userId = pathParameters[userIndex + 1];
        if (requestUser.id === userId) {
          isOwner = true;
        }
      }
      if (!isOwner) {
        // check whether user have private file permission
        await sequelize.transaction(async (transaction) => {
          const userPermissions = await permissionAction.getUserAllPermission(ctx, requestUser.id, transaction);
          if (!userPermissions || !Object.keys(userPermissions).length) {
            throw new PermissionDenied(translate('error:user_without_any_permission'));
          }
          if (!userPermissions['private-file'] || userPermissions['private-file'] === 'banned') {
            throw new PrivateFileProtected();
          }
        });
      }
    }
  } else {
    const isResourceFile = path.includes('/resources/');
    const apiPath = (apiDetail?.path) || null;
    const resourcePermissions = await permissionAction.getResourcePermissions(ctx, apiPath || path, method);

    if (resourcePermissions?.length) {
      let userId: string;
      let session;
      if (isResourceFile) {
        session = currentSession || cookieSession;
        userId = (currentUser?.id) || (cookieUser?.id);
      } else {
        session = currentSession;
        userId = (currentUser?.id) || '';
      }
      if (!session || !userId) {
        throw new LoginRequired();
      }
      let userPermissions;
      await sequelize.transaction(async (transaction) => {
        userPermissions = await permissionAction.getUserAllPermission(ctx, userId, transaction);
      });
      if (!userPermissions || !Object.keys(userPermissions).length) {
        throw new PermissionDenied(translate('error:user_without_any_permission'));
      }
      let sessionPermissions;
      if (session.type === 'login') {
        sessionPermissions = userPermissions;
      } else {
        sessionPermissions = permissionAction.removeOverSessionPermission(session.session_permissions, userPermissions);
      }
      const needPermissions = [];
      let success = false;
      for (const resourcePermission of resourcePermissions) {
        const result = permissionAction.comparePermission(resourcePermission, sessionPermissions);
        if (result.success) {
          success = true;
          break;
        } else {
          needPermissions.push(result.needPermission);
        }
      }
      if (!success) {
        throw new PermissionDenied({ needPermissions });
      }
    }
  }
  await next();
};

export default permissionHandler;
