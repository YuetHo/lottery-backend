import { Next } from 'koa';
import Joi, { Schema } from 'joi';
import joiAction from '@actions/joi';
import { HttpContext } from '@http/interface';
import { convertObjToTrueObj, convertTrueObjToObj } from '@utils/object-convertor';

const tryConvertJson = (text: string) => {
  try {
    if (typeof text === 'string' && /^({|\[).*(}|\])$/g.test(text)) {
      return JSON.parse(text);
    }
    return text;
  } catch (err) {
    return text;
  }
};

const getRequestData = (ctx: HttpContext, key: string) => {
  const { request, params } = ctx;
  switch (key) {
    case 'body':
      return request.body;
    case 'query':
      return request.query;
    case 'cookies':
      return request.body;
    case 'params':
      return params;
    default:
      return null;
  }
};

const apiValidator = (key: 'body' | 'query' | 'params' | 'cookies', schema: any) => async (ctx: HttpContext, next: Next) => {
  const { request } = ctx;
  const { query } = request;
  const { languageId } = query;
  const trueRequest = convertObjToTrueObj('', getRequestData(ctx, key));
  for (const itemKey of Object.keys(trueRequest)) {
    trueRequest[itemKey] = tryConvertJson(trueRequest[itemKey]);
  }
  const requestObj = convertTrueObjToObj(trueRequest);
  const result = await joiAction.validate(ctx, Array.isArray(languageId) ? languageId[0] : languageId, schema, requestObj);
  const { validatedData } = result;
  ctx.validatedData = ctx.validatedData || {};
  ctx.validatedData[key] = validatedData;
  await next();
};

apiValidator.body = (schema: { [key: string]: Schema }) => apiValidator('body', Joi.object().keys(schema));
apiValidator.query = (schema: { [key: string]: Schema }) => apiValidator('query', Joi.object().keys({
  ...schema,
  languageId: Joi.string(),
}));
apiValidator.params = (schema: { [key: string]: Schema }) => apiValidator('params', Joi.object().keys(schema));
apiValidator.cookies = (schema: { [key: string]: Schema }) => apiValidator('cookies', Joi.object().keys(schema));

export default apiValidator;
