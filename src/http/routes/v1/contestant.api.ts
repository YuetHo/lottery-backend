import { API } from '@http/interface';
import ContestantController from '@http/controllers/contestant';

const apiList: API[] = [
  {
    method: 'post',
    path: '/contestant/register',
    controllerFunc: ContestantController.register,
  },
  {
    method: 'post',
    path: '/contestant/login',
    controllerFunc: ContestantController.login,
  },
  {
    method: 'delete',
    path: '/contestant/logout',
    controllerFunc: ContestantController.logout,
    sessionRequire: true,
  },
];

export default apiList;
