import { API } from '@http/interface';
import QueryController from '@http/controllers/query';

const apiList: API[] = [
  {
    method: 'get',
    path: '/query/type',
    permissions: [{ query: 'read' }],
    controllerFunc: QueryController.getQueryTypes,
  },
  {
    method: 'get',
    path: '/query/list',
    permissions: [{ query: 'read' }],
    controllerFunc: QueryController.getQueryList,
  },
  {
    method: 'post',
    path: '/query',
    permissions: [{ query: 'write' }],
    controllerFunc: QueryController.createQuery,
  },
  {
    method: 'patch',
    path: '/query/:queryId',
    permissions: [{ query: 'write' }],
    controllerFunc: QueryController.updateQuery,
  },
];

export default apiList;
