import { API } from '@http/interface';
import RoleController from '@http/controllers/role';

const apiList: API[] = [
  {
    method: 'get',
    path: '/role',
    permissions: [{ permission: 'read' }],
    controllerFunc: RoleController.getRoles,
  },
  {
    method: 'get',
    path: '/role/list',
    permissions: [{ role: 'read' }],
    controllerFunc: RoleController.getRoleList,
  },
  {
    method: 'post',
    path: '/role',
    permissions: [{ role: 'write' }],
    controllerFunc: RoleController.createRole,
  },
  {
    method: 'patch',
    path: '/role/:roleId',
    permissions: [{ role: 'write' }, { user: 'read', translation: 'read' }],
    controllerFunc: RoleController.updateRole,
  },
  {
    method: 'patch',
    path: '/role/user/:userId',
    permissions: [{ role: 'write', user: 'write' }],
    controllerFunc: RoleController.assignUserRoles,
  },
  {
    method: 'delete',
    path: '/role/:roleId',
    permissions: [{ role: 'write' }],
    controllerFunc: RoleController.deleteRole,
  },
];

export default apiList;
