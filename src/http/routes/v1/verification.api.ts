import { API } from '@http/interface';
import VerificationController from '@http/controllers/verification';

const apiList: API[] = [
  {
    method: 'post',
    path: '/verification/send',
    controllerFunc: VerificationController.sendCode,
  },
  {
    method: 'post',
    path: '/verification/verify',
    controllerFunc: VerificationController.verifyCode,
  },
];

export default apiList;
