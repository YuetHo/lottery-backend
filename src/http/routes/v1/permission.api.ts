import { API } from '@http/interface';
import PermissionController from '@http/controllers/permission';

const apiList: API[] = [
  {
    method: 'get',
    path: '/permission',
    permissions: [{ permission: 'read' }],
    controllerFunc: PermissionController.getPermissions,
  },
  {
    method: 'get',
    path: '/permission/user/:userId',
    permissions: [{ permission: 'read', user: 'read' }],
    controllerFunc: PermissionController.getUserPermissions,
  },
  {
    method: 'patch',
    path: '/permission/user/:userId',
    permissions: [{ permission: 'write', user: 'write' }],
    controllerFunc: PermissionController.assignUserPermissions,
  },
  {
    method: 'get',
    path: '/permission/role/:roleId',
    permissions: [{ permission: 'read', role: 'read' }],
    controllerFunc: PermissionController.getRolePermissions,
  },
  {
    method: 'patch',
    path: '/permission/role/:roleId',
    permissions: [{ permission: 'write', role: 'write' }],
    controllerFunc: PermissionController.assignRolePermissions,
  },
  {
    method: 'get',
    path: '/permission/list',
    permissions: [{ permission: 'read' }],
    controllerFunc: PermissionController.getPermissionList,
  },
  {
    method: 'post',
    path: '/permission',
    permissions: [{ permission: 'write' }],
    controllerFunc: PermissionController.createPermission,
  },
  {
    method: 'patch',
    path: '/permission/:permissionId',
    permissions: [{ permission: 'write' }],
    controllerFunc: PermissionController.updatePermission,
  },
  {
    method: 'delete',
    path: '/permission/:permissionId',
    permissions: [{ permission: 'write' }],
    controllerFunc: PermissionController.deletePermission,
  },
];

export default apiList;
