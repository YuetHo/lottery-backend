import { API } from '@http/interface';
import TicketController from '@http/controllers/ticket';

const apiList: API[] = [
  {
    method: 'get',
    path: '/ticket',
    controllerFunc: TicketController.buyTicket,
    sessionRequire: true,
  },
];

export default apiList;
