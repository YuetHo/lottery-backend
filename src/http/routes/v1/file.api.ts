import { API } from '@http/interface';
import FileController from '@http/controllers/file';

const apiList: API[] = [
  {
    method: 'post',
    path: '/file/temporary',
    controllerFunc: FileController.uploadTemporaryFile,
  },
  {
    method: 'post',
    path: '/file/temporary/image',
    controllerFunc: FileController.uploadTemporaryImage,
  },
  {
    method: 'post',
    path: '/file/temporary/video',
    controllerFunc: FileController.uploadTemporaryVideo,
  },
  { // for html editor
    method: 'post',
    path: '/file/html-image',
    controllerFunc: FileController.uploadHtmlImage,
  },
];

export default apiList;
