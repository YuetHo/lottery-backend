import { API } from '@http/interface';
import ResourceController from '@http/controllers/resource';

const apiList: API[] = [
  {
    method: 'get',
    path: '/resource/list',
    permissions: [{ resource: 'read' }],
    controllerFunc: ResourceController.getResourceList,
  },
  {
    method: 'patch',
    path: '/resource/:resourceId',
    permissions: [{ resource: 'write' }],
    controllerFunc: ResourceController.updateResource,
  },
  {
    method: 'delete',
    path: '/resource/:resourceId',
    permissions: [{ resource: 'write' }],
    controllerFunc: ResourceController.deleteResource,
  },
];

export default apiList;
