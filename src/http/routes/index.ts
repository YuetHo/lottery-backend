import { Next } from 'koa';
import Router from '@koa/router';
import v1List from '@http/routes/v1';
import devList from '@http/routes/dev';
import { API, ApiRecord } from '@http/interface';
import sessionHandler from '@http/middleware/session-handler';
import permissionHandler from '@http/middleware/permission-handler';

const router = new Router();

const apisDetail: ApiRecord[] = [];

const defineAPI = (header: string, apis: API[]) => {
  for (const api of apis) {
    const { method, path, permissions, controllerFunc, sessionRequire } = api;
    const fullPath = header ? `${header}${path}` : path;
    const apiDetail = {
      method,
      path: fullPath,
      sessionRequire,
      permissions,
    };
    apisDetail.push(apiDetail);
    router[method](fullPath, async (ctx: any, next: Next) => {
      await sessionHandler(apiDetail)(ctx, async () => {});
      await permissionHandler(apiDetail)(ctx, async () => {});
      await controllerFunc(ctx, next);
    });
  }
};

defineAPI('/v1', v1List);
if (process.env.NODE_ENV === 'development') {
  defineAPI('/dev', devList);
}

export { router };

export default { router, apisDetail };
