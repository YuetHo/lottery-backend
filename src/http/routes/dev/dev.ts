import { API } from '@http/interface';
import DevController from '@http/controllers/dev';

const apiList: API[] = [
  {
    method: 'delete',
    path: '/contestant/delete',
    controllerFunc: DevController.deleteContestant,
  },
  {
    method: 'post',
    path: '/draw/create',
    controllerFunc: DevController.createDraw,
  },
  {
    method: 'patch',
    path: '/draw/settle',
    controllerFunc: DevController.settleDraw,
  },
  {
    method: 'delete',
    path: '/draw/delete',
    controllerFunc: DevController.deleteDraw,
  },
];

export default apiList;
