import Koa, { Next } from 'koa';
import Logger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import koaStatic from 'koa-static';
import mount from 'koa-mount';
import userAgent from 'koa-useragent';
import cors from '@koa/cors';
import path from 'path';
import initContext from '@utils/context';
import { initTranslationSource } from '@utils/translation';
import errorHandler from '@http/middleware/error-handler';
import sessionHandler from '@http/middleware/session-handler';
import { HttpContext } from '@http/interface';
import permissionHandler from '@http/middleware/permission-handler';
import { router } from '@http/routes';

const { REQUEST_TIME_OVER_LOG } = process.env;
const requestTimeLog = Number.isNaN(Number(REQUEST_TIME_OVER_LOG)) ? 1000 : Number(REQUEST_TIME_OVER_LOG);

const app = new Koa();
initTranslationSource();
app.use(Logger((str, arg: any[]) => {
  console.log(str);
  const method = arg[1];
  const apiPath = arg[2];
  const time = arg[4];
  if (time) {
    const timeCount = Number(time.replace('ms', ''));
    if (!Number.isNaN(timeCount) && timeCount > requestTimeLog) {
      console.log(`Request Time Over ${requestTimeLog}ms: ${method} ${apiPath} ${time}`);
    }
  }
}));
app.use(bodyParser());
app.use(userAgent);
app.use(cors({
  allowHeaders: ['Content-Type', 'x-access-token', 'Accept', 'Authorization', 'x-requested-with'],
  credentials: true,
}));
app.use(errorHandler());
app.use(async (ctx: HttpContext, next: Next) => {
  const { request } = ctx;
  const { query } = request;
  const { languageId } = query;
  await initContext(ctx, { languageId: Array.isArray(languageId) ? languageId[0] : languageId });
  await next();
});

app.use(router.routes()).use(router.allowedMethods());
app.use(sessionHandler());
app.use(permissionHandler());
app.use(mount('/upload', koaStatic(path.join(__dirname, '../../upload'))));
app.use(mount('/resources/public', koaStatic(path.join(__dirname, '../../resources/public'))));

export default app;
