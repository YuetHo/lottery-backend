import RemoteEventEmitter from '@lib/redis/remote-event-emitter';

const event = new RemoteEventEmitter();

export default event;
