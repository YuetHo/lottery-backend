import path from 'path';
import fileAction from '@actions/file';
import { API } from '@server-event/interface';

const currentFolder = path.join(__dirname, '');
const files = fileAction.loadFolderFiles(currentFolder, ['index.ts', 'index.js']);

const apiList: API[] = [];

for (const fileName of Object.keys(files)) {
  apiList.push(...files[fileName]);
}

export default apiList;
