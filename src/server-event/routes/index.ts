import Route from 'route-parser';
import v1List from '@server-event/routes/v1';
import devList from '@server-event/routes/dev';
import { API, ApiRecord } from '@server-event/interface';

export const apisDetail: ApiRecord[] = [];

const defineAPI = (header: string, apis: API[]) => {
  for (const api of apis) {
    const { path, uniqueKey, sessionRequire, permissions, joinController, leaveController } = api;
    const fullPath = header ? `${header}${path}` : path;
    const apiDetail = {
      method: 'EVENT',
      path: fullPath,
      uniqueKey,
      sessionRequire,
      permissions,
      joinController,
      leaveController,
    };
    apisDetail.push(apiDetail);
  }
};

defineAPI('/v1', v1List);
if (process.env.NODE_ENV === 'development') {
  defineAPI('/dev', devList);
}

export const findMatchApi = (requestUrl: string) => {
  for (const api of apisDetail) {
    const { path, uniqueKey } = api;
    if (uniqueKey) {
      const pathRoute = `${path}/:${uniqueKey}`;
      const route = new Route(pathRoute);
      const requestParams = route.match(requestUrl);
      if (requestParams) {
        return {
          ...api,
          params: requestParams,
        };
      }
    } else if (requestUrl === path) {
      return api;
    }
  }
  return null;
};

export default { apisDetail, findMatchApi };
