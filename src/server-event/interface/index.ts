import { PermissionGroup } from '@utils/interface';

export interface API {
  path: string
  uniqueKey?: string;
  sessionRequire?: boolean;
  permissions?: PermissionGroup[];
  joinController ?: Function;
  leaveController ?: Function;
}

export interface ApiRecord {
  method: string;
  path: string;
  params ?: any;
  uniqueKey?: string;
  sessionRequire?: boolean;
  permissions?: PermissionGroup[];
  joinController ?: Function;
  leaveController ?: Function;
}
