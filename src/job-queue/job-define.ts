import '@utils/config';
import path from 'path';
import Bull, { Queue, QueueOptions } from 'bull';
import fileAction from '@actions/file';

const { REDIS_PORT, REDIS_HOST } = process.env;

const currentFolder = path.join(__dirname, './jobs');
const jobList = fileAction.loadFolderFiles(currentFolder, [], false);

const redisConfig: QueueOptions = {
  redis: {
    port: Number(REDIS_PORT) || 6379,
    host: REDIS_HOST || '127.0.0.1',
  },
};

const jobs: {
  [jobId: string]: Queue;
} = {};

for (const jobId of jobList.map((jobFile: string) => jobFile.split('.')[0])) {
  jobs[jobId] = new Bull(jobId, redisConfig);
}

export default jobs;
