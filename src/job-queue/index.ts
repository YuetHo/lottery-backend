import '@utils/config';
import path from 'path';
import Bull, { QueueOptions } from 'bull';
import fileAction from '@actions/file';

const { REDIS_PORT, REDIS_HOST } = process.env;

const currentFolder = path.join(__dirname, './jobs');
const jobs = fileAction.loadFolderFiles(currentFolder);

const redisConfig: QueueOptions = {
  redis: {
    port: Number(REDIS_PORT) || 6379,
    host: REDIS_HOST || '127.0.0.1',
  },
};

const start = () => {
  for (const jobId of Object.keys(jobs)) {
    const queue = new Bull(jobId, redisConfig);
    queue.process(async (job, done) => jobs[jobId](job, done));
  }
};

start();
console.log('Start Job Queue');
