import { Job, DoneCallback } from 'bull';
import { parse } from 'json2csv';
import initContext from '@utils/context';
import exportOptionList from '@utils/export-option-list';
import { getResult } from '@actions/list';
import jobs from '@job-queue/job-define';
import { NotFoundError } from '@utils/errors';

const getListExport = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  const { optionId, query, contextOption, target, fileName } = data;
  const ctx = await initContext(null, contextOption);
  try {
    const option = exportOptionList[optionId];
    console.log('Export option:', optionId);
    if (!option) {
      throw new NotFoundError({ type: 'Export option', resource: optionId });
    }
    const { exportSetting } = option;
    const { dataReconstruct } = exportSetting;
    const { translate, languageId } = ctx;
    let results = await getResult(ctx, option, query);
    if (dataReconstruct && Array.isArray(results)) {
      results = dataReconstruct(ctx, results);
    }
    let csv;
    if (Array.isArray(results) && results.length) {
      const example = results[0];
      const fields = Object.keys(example);
      try {
        csv = parse(results, { fields });
      } catch (err) {
        console.error('Export csv error:', err);
      }
    }
    jobs.send_email.add({
      receivers: [target],
      subject: translate('export_email_title'),
      content: translate('export_email_content'),
      attachments: [{
        filename: `${fileName}.csv`,
        content: csv,
      }],
      languageId,
    });
  } catch (error) {
    console.log('Get list export Error:', error);
    job.moveToFailed(data, true);
  }
  done();
};

export default getListExport;
