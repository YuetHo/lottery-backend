import Mustache from 'mustache';
import { Job, DoneCallback } from 'bull';
import { Op, WhereOptions, QueryTypes } from 'sequelize';
import initContext from '@utils/context';
import { Context } from '@utils/interface';
import sequelize from '@database';
import translationAction from '@actions/translation';
import messageHandlers from '@actions/handler/message';
import jobs from '@job-queue/job-define';

const { DEFAULT_LANGUAGE } = process.env;

export interface Target {
  userId?: string;
  fcms?: any[];
  [key: string]: any;
}

const getUserInfo = async (ctx: Context, userIds?: string[]) => {
  const { models } = ctx;
  const { User, FCM } = models;
  // result.map(record => record.id)
  const targets = [];
  const filter: WhereOptions = {};
  if (userIds) {
    filter.id = userIds;
  }
  const users = await User.findAll({
    attributes: ['id', 'email', 'phone', 'languageId'],
    include: [
      {
        model: FCM,
        required: false,
      },
    ],
    where: filter,
  });
  targets.push(...users.map((user) => ({
    ...user.toJSON(),
    userId: user.id,
    fcms: user.fcms?.map((fcm) => fcm.toJSON()),
  })));
  return targets;
};

const getTarget = async (ctx: Context, type: string, reference?: any) => {
  const { models } = ctx;
  const { User, FCM, UserRole, Query } = models;
  let targets: Target[] = [];
  if (type === 'all') {
    const users = await User.findAll({
      attributes: ['id', 'email', 'phone', 'languageId'],
      include: [
        {
          model: FCM,
          required: false,
        },
      ],
    });
    for (const user of users) {
      targets.push({
        ...user.toJSON(),
        userId: user.id,
        fcms: user.fcms?.map((fcm) => fcm.toJSON()),
      });
    }
    const fcms = await FCM.findAll({
      where: {
        userId: {
          [Op.is]: null,
        },
      },
    });
    if (fcms?.length) {
      targets.push({
        fcms: fcms.map((fcm) => fcm.toJSON()),
      });
    }
  } else if (type === 'all_user') {
    const selectedTargets = await getUserInfo(ctx);
    targets.push(...selectedTargets);
  } else if (type === 'all_guest') {
    const fcms = await FCM.findAll({
      where: {
        userId: {
          [Op.is]: null,
        },
      },
    });
    if (fcms?.length) {
      targets.push({
        fcms: fcms.map((fcm) => fcm.toJSON()),
      });
    }
  } else if (type === 'user') {
    const { queryId, variableValues } = reference;
    const query = await Query.findOne({
      where: {
        id: queryId,
      },
    });
    if (query) {
      const records: { id: string }[] = await sequelize.query(query.content, { replacements: variableValues, type: QueryTypes.SELECT });
      if (records?.length) {
        const selectedTargets = await getUserInfo(ctx, records.map((record) => record?.id));
        targets.push(...selectedTargets);
      }
    }
  } else if (type === 'fcm_key') {
    targets = [{ fcms: [reference] }];
  } else if (type === 'role') {
    const users = await User.findAll({
      attributes: ['id', 'email', 'phone', 'languageId'],
      include: [
        {
          model: UserRole,
          required: true,
          where: {
            roleId: reference,
          },
        },
        {
          model: FCM,
          required: false,
        },
      ],
    });
    targets.push(...users.map((user) => ({
      ...user.toJSON(),
      userId: user.id,
      fcms: user.fcms?.map((fcm) => fcm.toJSON()),
    })));
  } else if (type === 'userIds') {
    const selectedTargets = await getUserInfo(reference);
    targets.push(...selectedTargets);
  }
  return targets;
};

const getMessage = async (ctx: Context, messageId: string) => {
  const { models } = ctx;
  const { Message, Translation } = models;
  const message = await Message.findOne({
    where: {
      id: messageId,
    },
    include: [
      {
        model: Translation,
        where: {
          model: 'Message',
        },
        association: Message.hasMany(Translation,
          {
            foreignKey: 'referenceId', sourceKey: 'id',
          }),
      },
    ],
  });
  if (message) {
    const formattedMessage = translationAction.translationHandler(message.toJSON());
    return formattedMessage;
  }
  return null;
};

const createNotification = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  const ctx = await initContext();
  let targets: Target[] = [];
  try {
    for (const targetInfo of data.targets) {
      const { type, reference } = targetInfo;
      const target = await getTarget(ctx, type, reference);
      targets = [...targets, ...target];
    }
    for (const content of data.contents) {
      const { type, messageId, messageParams, customContent = {} } = content;

      const notifyInfos = [...targets];
      for (let i = 0; i < notifyInfos.length; i += 1) {
        const { userId, phone, email, fcms = [], languageId } = notifyInfos[i];
        const handlerValues: { [key: string]: any } = {};
        // Call handler to get the real message
        const messageContent: { title?: string, content?: string; imageContent?: string } = {};
        const message = await getMessage(ctx, messageId);
        if (message) {
          if (message.handlers) {
            for (const handlerId of message.handlers) {
              const selectedHandler = messageHandlers[handlerId];
              const params = { ...messageParams[handlerId] };
              if (params?.userId === 'system') {
                params.userId = userId;
              }
              ctx.languageId = languageId;
              const handlerValue = await selectedHandler.call(ctx, params);
              if (handlerValue) {
                for (const key of Object.keys(handlerValue)) {
                  if (handlerValue[key] && typeof handlerValue[key] === 'object' && type !== 'push') {
                    handlerValues[`${handlerId}_${key}`] = handlerValue[key][languageId || DEFAULT_LANGUAGE];
                  } else {
                    handlerValues[`${handlerId}_${key}`] = handlerValue[key];
                  }
                }
              }
            }
          }
          const selectedLanguage = languageId || DEFAULT_LANGUAGE;
          const msgContent = message.content?.[selectedLanguage]?.value;
          const msgImgContent = message.imageContent?.[selectedLanguage]?.value;
          messageContent.title = message.title[languageId || DEFAULT_LANGUAGE].value;
          if (msgContent) {
            messageContent.content = Mustache.render(msgContent, handlerValues);
          }
          if (msgImgContent) {
            messageContent.imageContent = msgImgContent;
          }
        }
        switch (type) {
          case 'sms':
            if (phone) {
              jobs.send_sms.add({
                phone,
                message: messageContent.content,
              });
            }
            break;
          case 'email':
            if (email) {
              jobs.send_email.add({
                receivers: [email],
                subject: customContent.title || messageContent.title,
                content: customContent.content || messageContent.content,
                attachments: (messageContent.imageContent && [messageContent.imageContent]) || [],
                languageId,
              });
            }
            break;
          case 'inbox':
            if (userId) {
              jobs.send_message_inbox.add({
                target: userId,
                ...messageContent,
                ...customContent,
                languageId,
              });
            }
            break;
          case 'push':
            for (const fcm of fcms) {
              let fcmMessageContent = {};
              if (message) {
                const messageValue: { [key: string]: any } = {};
                for (const key of Object.keys(handlerValues)) {
                  if (handlerValues[key] && typeof handlerValues[key] === 'object') {
                    messageValue[key] = handlerValues[key][fcm.languageId];
                  } else {
                    messageValue[key] = handlerValues[key];
                  }
                }
                fcmMessageContent = {
                  title: message.title[fcm.languageId || DEFAULT_LANGUAGE].value,
                  content: Mustache.render(message.content[fcm.languageId || DEFAULT_LANGUAGE].value, messageValue),
                };
              }
              jobs.push_notify.add({
                targets: [fcm.key],
                message: {
                  ...fcmMessageContent,
                  ...customContent,
                },
              });
            }
            break;
          default:
            break;
        }
      }
    }
  } catch (error) {
    console.log('Notification Error:', error);
    job.moveToFailed(data, true);
  }
  console.log('Notification Processing!', targets);
  done();
};

export default createNotification;
