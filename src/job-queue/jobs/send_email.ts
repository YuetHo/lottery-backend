import { Job, DoneCallback } from 'bull';
import emailAction from '@actions/email';
import initContext from '@utils/context';

const sendEmail = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  const { receivers, subject, content, templateId, attachments, languageId } = data;
  const ctx = await initContext(undefined, { languageId });
  console.log(`Send email to ${receivers} with title: ${subject}`);
  try {
    await emailAction.sendEmail(ctx, [receivers], {
      subject, content,
    }, templateId, attachments);
  } catch (error) {
    console.log('Send email Error:', error);
    job.moveToFailed(data, true);
  }
  done();
};

export default sendEmail;
