import { Job, DoneCallback } from 'bull';
import drawAction from '@actions/draw';

const settlement = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  const { drawId } = data;
  try {
    await drawAction.settle(drawId);
  } catch (error) {
    console.log('Draw Settlement Error:', error);
    job.moveToFailed(data, true);
  }
  done();
};

export default settlement;
