import { Schedule } from '@workers/interface';
import drawController from '@workers/controllers/draw';

const schedules: Schedule[] = [
  {
    scheduleTime: '0 */5 * * * *',
    controllerFunc: drawController.createDraw,
  },
];

export default schedules;
