import path from 'path';
import fileAction from '@actions/file';
import { Schedule } from '@workers/interface';

const currentFolder = path.join(__dirname, '');
const files = fileAction.loadFolderFiles(currentFolder, ['index.ts', 'index.js']);

const schedules: Schedule[] = [];

for (const fileName of Object.keys(files)) {
  schedules.push(...files[fileName]);
}

export default schedules;
