import '@utils/config';
// import schedule from 'node-schedule';
// import scheduleJobs from '@workers/schedule-jobs';
// import initContext from '@utils/context';
import constantAction from '@actions/constant';
import drawAction from '@actions/draw';

const createDraw = (delay: number) => {
  setTimeout(async () => {
    const constant = await constantAction.getConstants();
    await drawAction.createDraw(Number(constant.draw_period) * 1000);
    return createDraw(Number(constant.draw_period) * 1000);
  }, delay);
};

const init = async () => {
  // const ctx = await initContext();
  // for (const scheduleJob of scheduleJobs) {
  //   const { scheduleTime, controllerFunc } = scheduleJob;
  //   schedule.scheduleJob(scheduleTime, () => controllerFunc(ctx));
  // }
  const constant = await constantAction.getConstants();
  createDraw(Number(constant.draw_period) * 1000);
};

init();
