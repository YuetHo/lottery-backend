import drawAction from '@actions/draw';
import constantAction from '@actions/constant';

export default {
  createDraw: async () => {
    const constant = await constantAction.getConstants();
    await drawAction.createDraw(Number(constant.draw_period) * 1000);
  },
};
