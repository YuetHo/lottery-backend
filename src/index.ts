import '@utils/config';
import Http from 'http';
import httpApp from '@http';
import socketApp from '@socket';
import httpRoute from '@http/routes';
import eventRoute from '@server-event/routes';
import socketRoute from '@socket/routes';
import resourceChecker from '@utils/resource-checker';
import folder from '@utils/folder';

const { PORT } = process.env;

const server = new Http.Server(httpApp.callback());
const io = require('socket.io')(server, {
  cors: {
    // origin: 'http://localhost:3000',
    // methods: ['GET', 'POST'],
    // allowedHeaders: ['Access-Control-Allow-Origin'],
    // credentials: true,
  },
});

socketApp.init(io);
resourceChecker([
  ...httpRoute.apisDetail,
  ...eventRoute.apisDetail,
  ...socketRoute.apisDetail,
]);

server.listen(process.env.PORT, async () => {
  await folder.folderSetup();
  console.log(`the server is start at port ${PORT}`);
});
