import LRU from 'lru-cache';

const options = {
  length: (n: number, key: Array<any>) => n * 2 + key.length,
};

const LimitedLocalCache = new LRU({ ...options, maxAge: 60 * 60 * 1000 });
const LocalCache = new LRU(options);

export {
  LimitedLocalCache,
  LocalCache,
};
