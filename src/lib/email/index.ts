import path from 'path';
import nodemailer from 'nodemailer';
import { Attachment } from 'nodemailer/lib/mailer';
import EmailTemplate, { EmailOptions } from 'email-templates';

const { NODE_ENV, PREVIEW_EMAIL, SMTP_HOST, SMTP_USER, SMTP_PASSWORD, DEFAULT_EMAIL_SENDER } = process.env;

const templatesFolder = path.join(__dirname, '../../../resources/email-templates');
const assetFolder = path.join(templatesFolder, 'assets');

const enablePreview = !!Number(PREVIEW_EMAIL);

const transporter = nodemailer.createTransport({
  port: 587,
  host: SMTP_HOST,
  auth: {
    user: SMTP_USER,
    pass: SMTP_PASSWORD,
  },
});
const email = new EmailTemplate({
  message: {
    from: DEFAULT_EMAIL_SENDER,
  },
  transport: transporter,
  views: {
    root: templatesFolder,
    options: {
      extension: 'ejs',
    },
  },
  juice: true,
  juiceResources: {
    preserveImportant: true,
    webResources: {
      relativeTo: assetFolder,
      images: false,
    },
  },
  preview: NODE_ENV === 'development' && enablePreview,
  send: NODE_ENV === 'production',
});

const sendEmail = (options: EmailOptions) => {
  email.send(options);
};

export {
  EmailOptions,
  Attachment,
  sendEmail,
};
