import AccessYouSmsProvider from '@lib/sms/providers/access-you';
import TwilioSmsProvider from '@lib/sms/providers/twilio';

const { SMS_PROVIDERS } = process.env;

const availableSmsProviders = (SMS_PROVIDERS || '').split(',').map((provider) => provider.trim());

const providers: { [provider: string]: any } = {
  ACCESS_YOU: new AccessYouSmsProvider(),
  TWILIO: new TwilioSmsProvider(),
};

export default {
  async sendSms(phone: string, message: string, options = {}) {
    for (let i = 0; i < availableSmsProviders.length; i += 1) {
      const providerName = availableSmsProviders[i];
      const provider = providers[providerName];
      console.log(`sending sms to ${phone} with ${providerName}`);
      if (provider) {
        try {
          const result = await provider.sendSms(phone, message, options);
          return {
            success: true,
            provider: providerName,
            phone,
            message,
            options,
            retryTimes: i,
            rawResponse: result,
          };
        } catch (e) {
          console.warn(`sending sms to ${phone} with ${providerName} error:`, e);
        }
      }
    }

    return {
      success: false,
      provider: null,
      phone,
      message,
      options,
      retryTimes: availableSmsProviders.length,
      rawResponse: null,
    };
  },
};
