import Twilio from 'twilio';
import SmsProvider from '@lib/sms/SmsProvider';

const { TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN, TWILIO_MESSAGING_SERVICE_SID } = process.env;

export default class TwilioSmsProvider extends SmsProvider {
  async sendSms(phone: string, message: string) {
    phone = phone.replace(/ /g, ''); // remove space
    const client = Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
    const data = await client.messages
      .create({
        body: message,
        messagingServiceSid: TWILIO_MESSAGING_SERVICE_SID,
        to: phone,
      });
    console.log(`twilio sending SMS to ${phone}: ${message}, result:`, data);
    return data;
  }
}
