abstract class SmsProvider {
  // eslint-disable-next-line no-unused-vars
  abstract sendSms(phone: string, message: string): Promise<any>;
}

export default SmsProvider;
