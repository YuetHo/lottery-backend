import Redis from 'ioredis';
import '@utils/config';

const { PROJECT_ID, REDIS_PORT, REDIS_HOST } = process.env;

class RedisClient extends Redis {
  constructor() {
    super({
      port: Number(REDIS_PORT) || 6379,
      host: REDIS_HOST || '127.0.0.1',
      keyPrefix: PROJECT_ID,
    });
  }
}

export default RedisClient;
