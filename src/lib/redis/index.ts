import RedisClient from '@lib/redis/connection';

const redis = new RedisClient();

const toRedisKey = (groupKey: string, key: string) => `${groupKey}:${key}`;
const expire = async (groupKey: string, key: string, seconds: number) => {
  const result = await redis.expire(toRedisKey(groupKey, key), seconds);
  return result;
};
const exists = async (groupKey: string, keys: string | string[]) => {
  if (Array.isArray(keys)) {
    const checkKeys: string[] = keys.map((key) => toRedisKey(groupKey, key));
    const exist = await redis.exists(...checkKeys);
    return exist;
  }
  const checkKey: string = toRedisKey(groupKey, keys);
  const exist = await redis.exists(checkKey);
  return exist;
};

const get = async (groupKey: string, key: string) => {
  const redisKey = toRedisKey(groupKey, key);
  const value = await redis.get(redisKey);
  return value && JSON.parse(value);
};

export default {
  toRedisKey,
  expire,
  exists,
  incr: async (groupKey: string, key: string) => {
    const redisKey = toRedisKey(groupKey, key);
    const value = await redis.incr(redisKey);
    return JSON.parse(String(value));
  },

  set: async (groupKey: string, key: string, value: any, options?: { expire?: number }) => {
    const result = await redis.set(toRedisKey(groupKey, key), JSON.stringify(value));
    if (options?.expire) {
      expire(groupKey, key, options.expire);
    }
    return result;
  },

  get,
  // existGet: async (groupKey: string, key: string) => {
  //   const exist = await exists(groupKey, key);
  //   if (!exist) {
  //     return undefined;
  //   }
  //   const data = await get(groupKey, key);
  //   return data;
  // },

  mset: async (groupKey: string, keyObj: { [key: string]: any }) => {
    const redisKeyObj: { [key: string]: string } = {};
    for (const key of Object.keys(keyObj)) {
      redisKeyObj[toRedisKey(groupKey, key)] = JSON.stringify(keyObj[key]);
    }
    const result = await redis.mset(redisKeyObj);
    return result;
  },

  mget: async (groupKey: string, keyArray: string[]) => {
    const redisKeyArray = keyArray.map((key) => toRedisKey(groupKey, key));
    const result = await redis.mget(redisKeyArray);
    const jsonData = [];
    for (const record of result) {
      jsonData.push(record && JSON.parse(record));
    }
    return jsonData;
  },

  del: async (groupKey: string, keys: string | string[]) => {
    let redisKeys: string[] = [];
    redisKeys = Array.isArray(keys) ? keys.map((key) => toRedisKey(groupKey, key)) : [toRedisKey(groupKey, keys)];
    const result = await redis.del(redisKeys);
    return result;
  },
};
