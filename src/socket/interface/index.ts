import { Server, Socket } from 'socket.io';
import { Context, PermissionGroup } from '@utils/interface';

export interface SocketContext extends Socket, Context {
  connectAction: 'join' | 'leave';
  params ?: any;
  io: Server;
}

export interface API {
  path: string;
  sessionRequire ?: boolean;
  permissions ?: PermissionGroup[];
  controllerFunc ?: Function;
}

export interface ApiRecord {
  method: string;
  path: string;
  sessionRequire ?: boolean;
  permissions ?: PermissionGroup[];
  controllerFunc ?: Function;
}
