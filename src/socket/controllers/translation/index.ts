import Joi from 'joi';
import socketValidator from '@socket/middleware/socket-validator';
import { SocketContext } from '@socket/interface';

export default {
  async setLanguage(socket: SocketContext, params: any) {
    const { languageId } = await socketValidator(socket, params, {
      languageId: Joi.string().required(),
    });
    socket.languageId = languageId;
  },
};
