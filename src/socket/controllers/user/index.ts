import Joi from 'joi';
import moment from 'moment';
import socketValidator from '@socket/middleware/socket-validator';
import { AccessTokenExpired, NotFoundError } from '@utils/errors';
import { SocketContext } from '@socket/interface';

export default {
  async login(socket: SocketContext, params: any) {
    const validatedData = await socketValidator(socket, params, {
      accessToken: Joi.string().required(),
    });
    const { models, translate } = socket;
    const { User, SessionPermission, Session } = models;
    const { accessToken } = validatedData;
    const session = await Session.findOne({
      include: [
        {
          model: User,
          required: true,
        },
        {
          model: SessionPermission,
          required: false,
        },
      ],
      where: {
        accessToken,
      },
    });
    if (!session) {
      throw new NotFoundError({ type: 'session', resource: translate('session') });
    }
    if (session && moment().isAfter(moment(session.expiresAt))) {
      throw new AccessTokenExpired();
    }
    socket.currentSession = session;
    socket.currentUser = session.user;
  },
  async logout(socket: SocketContext) {
    socket.currentSession = undefined;
  },
};
