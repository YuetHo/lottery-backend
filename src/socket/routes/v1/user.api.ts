import { API } from '@socket/interface';
import UserController from '@socket/controllers/user';

const apiList: API[] = [
  {
    path: '/user/login',
    controllerFunc: UserController.login,
  },
  {
    path: '/user/logout',
    controllerFunc: UserController.logout,
    sessionRequire: true,
  },
];

export default apiList;
