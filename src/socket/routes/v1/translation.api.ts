import { API } from '@socket/interface';
import TranslationController from '@socket/controllers/translation';

const apiList: API[] = [
  {
    path: '/translation/language',
    controllerFunc: TranslationController.setLanguage,
  },
];

export default apiList;
