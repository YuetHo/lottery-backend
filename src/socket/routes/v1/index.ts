import path from 'path';
import { API } from '@socket/interface';
import fileAction from '@actions/file';

const currentFolder = path.join(__dirname, '');
const files = fileAction.loadFolderFiles(currentFolder, ['index.ts', 'index.js']);

const apiList: API[] = [];

for (const fileName of Object.keys(files)) {
  apiList.push(...files[fileName]);
}

export default apiList;
