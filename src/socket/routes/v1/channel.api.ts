import { API } from '@socket/interface';
import ChannelControllers from '@socket/controllers/channel';

const apiList: API[] = [
  {
    path: '/channel/subscribe',
    controllerFunc: ChannelControllers.subscribe,
  },
  {
    path: '/channel/unsubscribe',
    controllerFunc: ChannelControllers.unsubscribe,
  },
];

export default apiList;
