import { API, ApiRecord, SocketContext } from '@socket/interface';
import v1List from '@socket/routes/v1';
import devList from '@socket/routes/dev';
import errorHandler from '@socket/middleware/error-handler';
import permissionHandler from '@socket/middleware/permission-handler';

export const apisDetail: ApiRecord[] = [];

const defineAPI = (header: string, apis: API[]) => {
  for (const api of apis) {
    const { path, sessionRequire, permissions, controllerFunc } = api;
    const fullPath = header ? `${header}${path}` : path;
    const apiDetail = {
      path: fullPath,
      permissions,
      sessionRequire,
      controllerFunc,
      method: 'SOCKET',
    };
    apisDetail.push(apiDetail);
  }
};

defineAPI('/v1', v1List);
if (process.env.NODE_ENV === 'development') {
  defineAPI('/dev', devList);
}

export const router = {
  async init(socket: SocketContext) {
    for (const apiDetail of apisDetail) {
      const { path, controllerFunc } = apiDetail;
      // eslint-disable-next-line no-loop-func
      socket.on(path, async (msg, callBack) => {
        try {
          await permissionHandler(socket, apiDetail);
          console.log('Socket Called:', path, msg);
          if (controllerFunc) {
            const response = await controllerFunc(socket, msg);
            if (callBack) {
              callBack({
                status: 200,
                data: response,
              });
            }
          }
        } catch (e) {
          if (callBack) {
            const error = errorHandler(socket, e);
            callBack(error);
          }
        }
      });
    }
  },
};

export default { router, apisDetail };
