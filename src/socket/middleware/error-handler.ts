import { BaseError, InternalServerError } from '@utils/errors';
import sequelizeErrorHandler from '@utils/sequelize/error-handler';
import { SocketContext } from '@socket/interface';

export default (socket: SocketContext, defaultError: any) => {
  const { translate, languageId } = socket;
  console.log('Socket Error:', defaultError, languageId);
  let error = defaultError;
  error = sequelizeErrorHandler(socket, error);
  if (!(error instanceof BaseError)) {
    error = new InternalServerError();
    console.log('Internal Server Error:', defaultError);
  }
  const { status, code, extras } = error;
  return {
    status,
    data: {
      code,
      message: `${translate(`error:${code}`, extras)}`,
      extras,
    },
  };
};
