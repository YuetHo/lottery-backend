import { expect } from 'chai';
import apiClient, { AuthConfig } from '@test/utils/api-client';
import { testingContestant } from '@test/data/contestant';

describe('Testing draw function', () => {
  const authConfig: AuthConfig = { headers: {} };
  let createdDrawId: string;
  let boughtTicketId: string;
  before(async () => {
    await apiClient.post('/v1/contestant/register', testingContestant);
    const response = await apiClient.post('/v1/contestant/login', {
      email: testingContestant.email,
      password: testingContestant.password,
    });
    const { accessToken } = response.data;
    authConfig.headers.Authorization = accessToken;
  });
  it('create draw', async () => {
    const response = await apiClient.post('/dev/draw/create', {
      delay: 300 * 1000, // 5 mins
    });
    const { draw } = response.data;
    expect(draw).to.have.property('id');
    expect(draw).to.have.property('status');
    expect(draw.status).to.equal('processing');
    createdDrawId = draw.id;
  });
  it('buy ticket', async () => {
    const response = await apiClient.get('/v1/ticket', authConfig);
    expect(response.data).to.have.property('id');
    boughtTicketId = response.data.id;
  });
  it('settle draw', async () => {
    const response = await apiClient.patch('/dev/draw/settle', {
      drawId: createdDrawId,
    });
    const { draw } = response.data;
    expect(draw.status).to.equal('completed');
    expect(draw.winnerTicket).to.equal(boughtTicketId);
  });
  after(async () => {
    // await apiClient.delete('/dev/contestant/delete', {
    //   params: {
    //     email: testingContestant.email,
    //   },
    // });
    // await apiClient.delete('/dev/draw/delete', {
    //   params: {
    //     drawId: createdDrawId,
    //   },
    // });
  });
});
