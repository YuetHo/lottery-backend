import { expect } from 'chai';
import apiClient, { AuthConfig } from '@test/utils/api-client';
import { testingContestant } from '@test/data/contestant';

describe('Testing contestant API', () => {
  const authConfig: AuthConfig = { headers: {} };
  it('register', async () => {
    await apiClient.post('/v1/contestant/register', testingContestant);
  });
  it('login', async () => {
    const response = await apiClient.post('/v1/contestant/login', {
      email: testingContestant.email,
      password: testingContestant.password,
    });
    expect(response.data).to.have.property('accessToken');
    expect(response.data).to.have.property('refreshToken');
    expect(response.data).to.have.property('expiresAt');
    authConfig.headers.Authorization = response.data.accessToken;
  });
  it('logout', async () => {
    await apiClient.delete('/v1/contestant/logout', authConfig);
  });
  after(async () => {
    await apiClient.delete('/dev/contestant/delete', {
      params: {
        email: testingContestant.email,
      },
    });
  });
});
