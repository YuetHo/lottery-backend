type Contestant = {
  name: string,
  email: string,
  phone: string,
  password: string,
};

export const testingContestant: Contestant = {
  name: 'Adam Lee',
  email: 'adam.lee@gmail.com',
  phone: '97081321',
  password: 'Ab09214',
};

export const defaultContestant: Contestant = {
  name: 'May Fun',
  email: 'may.fun@gmail.com',
  phone: '69742531',
  password: 'Uf43216',
};
