import axios, { AxiosError } from 'axios';
import { expect, assert } from 'chai';
import '@test/utils/config';

const { PORT } = process.env;

export type AuthConfig = {
  headers: {
    Authorization?: string,
  },
};

export const baseURL = `http://localhost:${PORT}`;
const client = axios.create({
  baseURL,
});

const getErrorData = (apiError: AxiosError) => {
  const { response } = apiError;
  if (response) {
    const { data } = response;
    return data;
  }
  return null;
};

client.interceptors.response.use((response) => {
  expect(response.status).to.equal(200);
  return response;
}, (error: AxiosError) => {
  const errorData = getErrorData(error);
  if (errorData) {
    const { message } = errorData;
    assert.fail(message);
  }
  return Promise.reject(error);
});

export default client;
