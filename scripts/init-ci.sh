DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
defaultFile="$DIR/default/.gitlab-ci.yml.default"
currentFile="$DIR/../.gitlab-ci.yml"

# Reset file
if [[ $defaultFile ]]; then
rm -rf $currentFile
cp -i $defaultFile $currentFile
fi

while : ; do
    read -p "Enter stage domain: " STAGE_DOMAIN
    [[ $STAGE_DOMAIN = "" ]] || break
done

while : ; do
    read -p "Enter database user: " DB_USER
    [[ $DB_USER = "" ]] || break
done

while : ; do
    read -p "Enter database password: " DB_PASSWORD
    [[ $DB_PASSWORD = "" ]] || break
done


sed -i "" "s/{{STAGE_DOMAIN}}/$STAGE_DOMAIN/g; s/{{DB_USER}}/$DB_USER/g; s/{{DB_PASSWORD}}/$DB_PASSWORD/g" $currentFile
